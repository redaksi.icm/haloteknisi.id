<?php $this->load->view('header-front') ?>
<div class="row">
    <div class="col-sm-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$data[COL_POSTTITLE]?></h3>
            </div>
            <div class="box-body">
                <?php
                $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                ?>
                <div class="col-sm-12" style="display: flex; flex-flow: column wrap; text-align: center; margin: 10px 0px;">
                    <div class="lightBoxGallery" style="width: 100%">
                        <?php
                        if(count($files) > 1) {

                            foreach($files as $f) {
                                ?>
                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                                    <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-width: 50vh; margin: 5px; box-shadow: 0 1px 1px rgba(0,0,0,0.2)">
                                </a>
                            <?php
                            }
                        } else if(!empty($files[0])) {
                            ?>
                            <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                                <img src="<?=!empty($files[0][COL_FILENAME])?MY_UPLOADURL.$files[0][COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>" style="width: 100%; padding: 5px" />
                            </a>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <?=$data[COL_POSTCONTENT]?>
                <div class="small" style="text-align: right">
                    <button class="btn btn-white btn-xs" type="button"><i class="fa fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?></button>
                    <button class="btn btn-white btn-xs" type="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?=date("d M Y H:i", strtotime($data[COL_CREATEDON]))?></button>
                    <button class="btn btn-white btn-xs" type="button"><i class="fa fa-eye"></i>&nbsp;&nbsp;Dilihat <b><?=number_format($data[COL_TOTALVIEW], 0)?></b> kali</button>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     */
                    var disqus_config = function () {
                        this.page.url = '<?=site_url('post/view/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '<?=$this->setting_web_disqus_url?>';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
            <!-- /.box-footer-->
        </div>
    </div>
    <?php $this->load->view('sidebar-front') ?>
</div>
    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
<?php $this->load->view('footer-front') ?>