
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <!-- my css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">
    <style>
    .dropdown-item>p>.right {
      position: absolute;
      right: 1rem;
    }
    </style>
    <script>
        $(document).ready(function() {
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };

            $('.btn-login', $('.login-section')).click(function() {
                var form = $('#login-form');
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            toastr.error(data.error);
                        }else{
                            location.reload();
                        }
                    },
                    error : function(a,b,c){
                        toastr.error('Response Error');
                    }
                });
            });
        });
    </script>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_IMAGELOCATION] ? MY_UPLOADURL.$ruser[COL_NM_IMAGELOCATION] : MY_IMAGEURL.'user.jpg';
}
$req = $this->db
->where(($ruser[COL_ROLEID]==ROLEPROVIDER?COL_ID_PSIKOLOG:COL_ID_PATIENT), $ruser[COL_USERNAME])
->where(COL_ID_STATUS, ($ruser[COL_ROLEID]==ROLEPROVIDER?STATUS_MENUNGGU:STATUS_DITERIMA))
->count_all_results(TBL_T_APPOINTMENT);
?>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
        <div class="container">
            <a href="<?=site_url()?>" class="navbar-brand">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation" style="opacity: .8">
                <span class="brand-text font-weight-light"><?=$this->setting_web_name?></span>
            </a>
            <ul class="navbar-nav">

            </ul>

            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                  <a class="nav-link" href="#" title="Tentang Kami">
                    Hubungi Kami
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#" title="Tentang Kami">
                    FAQ
                  </a>
              </li>
                <?php
                if(IsLogin()) {
                    ?>
                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                          <img src="<?=$displaypicture?>" class="user-image img-circle elevation-1" alt="<?=$displayname?>" style="width: 20px !important; height: 20px !important">
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="<?=site_url('user/profile')?>" class="dropdown-item">Profil</a></li>
                        <li>
                          <a href="<?=site_url('schedule/index')?>" class="dropdown-item">
                            <p class="m-0">
                              Permintaan <?=$req>0?'<span class="right badge badge-danger">'.number_format($req).'</span>':''?>
                            </p>
                        </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li><a href="<?=site_url('user/changepassword')?>" class="dropdown-item"><i class="fad fa-key"></i>&nbsp;&nbsp;Ubah Password</a></li>
                        <li><a href="<?=site_url('user/logout')?>" class="dropdown-item"><i class="fad fa-sign-out"></i>&nbsp;&nbsp;Keluar</a></li>
                      </ul>
                    </li>

                    <!--<li class="nav-item dropdown user-menu">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="<?=$displaypicture?>" class="user-image img-circle elevation-1" alt="<?=$displayname?>" style="width: 20px !important; height: 20px !important">&nbsp;
                            <?=$displayname?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <li class="user-header bg-default">
                                <img src="<?=$displaypicture?>" class="img-circle elevation-2" alt="<?=$displayname?>">

                                <p>
                                    <?=$ruser[COL_NM_FULLNAME]?>
                                    <small><?=strtoupper($ruser[COL_ROLENAME])?></small>
                                </p>
                            </li>
                            <li class="user-footer">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('user/dashboard')?>" class="btn btn-primary"><i class="fad fa-tachometer-alt"></i> Dashboard</a>
                                <?php
                              } else {
                                ?>
                                <a href="<?=site_url('user/profile')?>" class="btn btn-outline-info"><i class="fad fa-user"></i> Profil</a>
                                <?php
                              }
                               ?>
                                <a href="<?=site_url('user/logout')?>" class="btn btn-outline-danger float-right"><i class="fad fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </li>-->
                    <?php
                } else {
                    ?>
                    <li class="nav-item user-menu">
                        <a class="nav-link text-success" href="<?=site_url('user/login')?>" title="Login">
                            <i class="fad fa-sign-in"></i><!--&nbsp;Login-->
                        </a>
                    </li>
                    <li class="nav-item user-menu">
                      <a class="nav-link text-primary" href="<?=site_url('user/register')?>" title="Daftar">
                          <i class="fa fa-user-plus"></i><!--&nbsp;Daftar-->
                      </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
