<?php
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('layouts/backend-header');
} else {
    $this->load->view('layouts/frontend-header');
}
?>
<?php
$user = GetLoggedUser();
$data = array();
$i = 0;
$no = 1;
$cols = array();

foreach ($res as $d) {
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_ID_APPOINTMENT] . '" />',
        anchor('schedule/edit/'.$d[COL_ID_APPOINTMENT], str_pad($d[COL_ID_APPOINTMENT], 5, '0', STR_PAD_LEFT)),
        date("Y-m-d", strtotime($d[COL_DATE_SCHEDULE])),
        $d[COL_NM_TYPE],
        anchor('schedule/status-edit/'.$d[COL_ID_APPOINTMENT],$d[COL_NM_STATUS],array('class' => 'modal-popup-status', 'data-status' => $d[COL_ID_STATUS], 'data-color' => $d[COL_NM_COLOR])),
        'Penyedia : '.$d["NM_Psikolog"].'<br  />Konsumen : '.$d["NM_Member"]
    );
  } else {
    $opt = '';
    $opt .= anchor('schedule/detail/'.$d[COL_ID_APPOINTMENT],'<i class="fad fa-eye"></i>');

    $res[$i] = array(
      $opt,
      str_pad($d[COL_ID_APPOINTMENT], 5, '0', STR_PAD_LEFT),
      date("Y-m-d", strtotime($d[COL_DATE_SCHEDULE])),
      $d[COL_NM_TYPE],
      $ruser[COL_ROLEID]==ROLEPROVIDER ? anchor('schedule/status-edit/'.$d[COL_ID_APPOINTMENT],$d[COL_NM_STATUS],array('class' => 'modal-popup-status', 'data-status' => $d[COL_ID_STATUS], 'data-color' => $d[COL_NM_COLOR])) : '<span class="badge" style="background-color: '.$d[COL_NM_COLOR].'; color: #000">'.strtoupper($d[COL_NM_STATUS]).'</span>',
      $ruser[COL_ROLEID]==ROLEPROVIDER ? anchor('user/detail/'.$d["ID_Member"], $d["NM_Member"], array("target"=>"_blank")) : anchor('user/detail/'.$d["ID_Provider"], $d["NM_Psikolog"], array("target"=>"_blank"))
    );
  }
  $i++;
  $no++;
}
$data = json_encode($res);
 ?>
<div class="content-header">
    <div class="<?=$user[COL_ROLEID]==ROLEADMIN?'container-fluid':'container'?>">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="<?=$user[COL_ROLEID]==ROLEADMIN?'container-fluid':'container'?>">
        <div class="row">
            <div class="col-sm-12">
              <?php
              if($ruser[COL_ROLEID]==ROLEADMIN) {
                ?>
                <p>
                    <?=anchor('schedule/delete','<i class="fa fa-trash"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('schedule/add','<i class="fa fa-plus"></i> CREATE',array('class'=>'btn btn-primary btn-sm'))?>
                </p>
                <?php
              }
              ?>
                <div class="card card-default">
                  <div class="card-header">
                    <?=form_open_multipart(current_url(), array('method'=>'get', 'role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group row">
                          <label class="control-label col-sm-12">Tanggal</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="DateFrom" value="<?=$filter['DateFrom']?>" required />
                          </div>
                          <label class="control-label col-sm-2 text-center">s.d</label>
                          <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="DateTo" value="<?=$filter['DateTo']?>" required />
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group row">
                          <label class="control-label col-sm-12">Status</label>
                          <div class="col-sm-12">
                            <select name="<?=COL_ID_STATUS?>" class="form-control">
                                <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS,$filter[COL_ID_STATUS],true,false,'-- Semua --')?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group row">
                          <label class="control-label col-sm-12">Layanan / Jasa</label>
                          <div class="col-sm-12">
                            <select name="<?=COL_ID_TYPE?>" class="form-control">
                                <option value="">-- Pilih Jenis Layanan / Jasa --</option>
                                <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_ID_TYPE, COL_NM_TYPE, (!empty($filter)?$filter[COL_ID_TYPE]:null))?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <!--<div class="col-sm-3">
                        <div class="form-group row">
                          <label class="control-label col-sm-12">Keyword</label>
                          <div class="col-sm-12">
                            <input type="text" class="form-control" name="Keyword" value="<?=$filter['Keyword']?>" placeholder="Kata Kunci" />
                          </div>
                        </div>
                      </div>-->
                    </div>
                    <div class="row">
                      <div class="col-sm-12 text-right">
                        <button type="reset" class="btn btn-sm btn-outline-secondary"><i class="fa fa-refresh"></i>&nbsp;RESET</button>
                        <button type="submit" class="btn btn-sm btn-outline-success"><i class="fa fa-filter"></i>&nbsp;FILTER</button>
                      </div>
                    </div>

                    <?=form_close()?>
                  </div>
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover">

                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-status" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Status</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times fa-sm"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <p class="text-danger error-message"></p>
        <form id="form-editor-status" method="post" action="#">
          <div class="form-group row">
            <label class="control-label col-sm-4">Status</label>
            <div class="col-sm-8">
              <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                  <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS)?>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Cancel</button>
        <button type="button" class="btn btn-primary btn-ok"><i class="fas fa-pencil"></i>&nbsp;Ubah</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('layouts/_js')?>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
          "autoWidth": false,
          //"sDom": "Rlfrtip",
          "aaData": <?=$data?>,
          //"bJQueryUI": true,
          //"aaSorting" : [[5,'desc']],
          "scrollY" : '32vh',
          "scrollX": "120%",
          "iDisplayLength": 10,
          "aLengthMenu": [[10, 100, 500, -1], [10, 100, 500, "Semua"]],
          "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
          "order": [[ 2, "desc" ]],
          "columnDefs": [
            {"targets": [0], "className": "nowrap"},
            {"targets": [1,2], "className": "nowrap"}
          ],
          "aoColumns": [
              <?=$ruser[COL_ROLEID]==ROLEADMIN?'{"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false}':'{"sTitle": "#", "width": "10px","bSortable":false}'?> ,
              {"sTitle": "No. Permintaan"},
              {"sTitle": "Tanggal"},
              {"sTitle": "Layanan / Jasa"},
              {"sTitle": "Status"},
              {"sTitle": "<?=$ruser[COL_ROLEID]==ROLEADMIN?'Penyedia / Konsumen':($ruser[COL_ROLEID]==ROLEPROVIDER?'Konsumen':'Penyedia')?>"}
          ],
          "createdRow": function(row, data, dataIndex) {
            //console.log(row);
            var color = $(data[4]).data('color');
            $(row).css("background-color", color);
          }
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
                console.log('clicked');
            }else{
                $('.cekbox').prop('checked',false);
            }
        });

        $("#modal-status").on("shown.bs.modal", function(){
          $("select", $("#modal-status")).select2({ width: 'resolve', theme: 'bootstrap4' });
        });
        $('.modal-popup-status').click(function(){
            var a = $(this);
            var status = $(this).data('status');
            var editor = $("#modal-status");

            $('[name=<?=COL_ID_STATUS?>]', editor).val(status);
            editor.modal("show");
            $(".btn-ok", editor).unbind('click').click(function() {
              var dis = $(this);
              dis.html("Loading...").attr("disabled", true);
              $('#form-editor-status').ajaxSubmit({
                  dataType: 'json',
                  url : a.attr('href'),
                  success : function(data){
                      if(data.error==0){
                          window.location.reload();
                      }else{
                          toastr.error(data.error);
                      }
                  },
                  error: function() {
                    toastr.error('Server Problem');
                  },
                  complete: function() {
                    dis.html('<i class="fas fa-pencil"></i>&nbsp;Ubah').attr("disabled", false);
                    editor.modal("hide");
                  }
              });
            });
            return false;
        });
    });
</script>
<?php
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('layouts/backend-footer');
} else {
    $this->load->view('layouts/frontend-footer');
}
?>
