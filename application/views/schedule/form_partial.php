<?php
if($isajax) {
  ?>
  <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'schedule-form','class'=>'form-horizontal'))?>
  <?php
}
?>
<div class="row">
    <div class="col-sm-6">
      <?php
      if($ruser[COL_ROLEID]!=ROLEPROVIDER) {
        ?>
        <div class="form-group row">
            <label class="control-label col-sm-4">Penyedia Jasa / Teknisi</label>
            <div class="col-sm-7">
              <?php
              if(!$forcepsikolog) {
                ?>
                <select name="<?=COL_ID_PSIKOLOG?>" class="form-control" required>
                    <option value="">-- Pilih Penyedia Jasa / Teknisi --</option>
                    <?=GetCombobox("SELECT ui.UserName, ui.NM_FullName from userinformation ui left join users u on u.UserName = ui.UserName where u.RoleID = ".ROLEPROVIDER." order by ui.NM_FullName", COL_USERNAME, COL_NM_FULLNAME, ($edit ? $data[COL_ID_PSIKOLOG] : null))?>
                </select>
                <?php
              } else {
                ?>
                <span style="font-italic"><?=!empty($data)&&!empty($data["NM_Psikolog"])?$data["NM_Psikolog"]:''?></span>
                <input type="hidden" name="<?=COL_ID_PSIKOLOG?>" value="<?=!empty($data)&&!empty($data[COL_ID_PSIKOLOG])?$data[COL_ID_PSIKOLOG]:''?>" />
                <?php
              }
              ?>
            </div>
        </div>
        <?php
      }
      ?>

      <div class="form-group row">
          <label class="control-label col-sm-4">Jenis Jasa</label>
          <div class="col-sm-7">
              <select name="<?=COL_ID_TYPE?>" class="form-control" required>
                <option>-- Pilih Jenis Layanan / Jasa --</option>
              </select>
          </div>
      </div>
      <?php
      if($ruser[COL_ROLEID]!=ROLEMEMBER) {
        ?>
        <div class="form-group row">
            <label class="control-label col-sm-4">Target</label>
            <div class="col-sm-7">
                <select name="<?=COL_ID_PATIENT?>" class="form-control" required>
                    <option value="">-- Pilih Target --</option>
                    <?=GetCombobox("SELECT ui.UserName, ui.NM_FullName from userinformation ui left join users u on u.UserName = ui.UserName where u.RoleID = ".ROLEMEMBER." order by ui.NM_FullName", COL_USERNAME, COL_NM_FULLNAME, ($edit ? $data[COL_ID_PATIENT] : null))?>
                </select>
            </div>
        </div>
        <?php
      }
      ?>
      <div class="form-group row">
          <label class="control-label col-sm-4">Tanggal</label>
          <div class="col-sm-3">
              <input type="text" class="form-control datepicker" name="<?=COL_DATE_SCHEDULE?>" value="<?= $edit ? $data[COL_DATE_SCHEDULE] : ""?>" required>
          </div>
      </div>
      <?php
      if($ruser[COL_ROLEID] == ROLEADMIN) {
          ?>
          <div class="form-group row">
              <label class="control-label col-sm-4">Status</label>
              <div class="col-sm-7">
                  <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                      <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS, ($edit ? $data[COL_ID_STATUS] : null))?>
                  </select>
              </div>
          </div>
          <?php
      }
      ?>
    </div>
    <div class="col-sm-6">
      <?php
      if($ruser[COL_ROLEID]!=ROLEPROVIDER) {
        ?>
        <div class="form-group row">
            <label class="control-label col-sm-4">Catatan / Keluhan</label>
            <div class="col-sm-8">
                <textarea class="form-control" rows="5" name="<?=COL_NOTE_PATIENT?>" required="true"><?= $edit ? $data[COL_NOTE_PATIENT] : ""?></textarea>
            </div>
        </div>
        <?php
      }
      ?>
      <?php
      if($ruser[COL_ROLEID]!=ROLEMEMBER) {
        ?>
        <div class="form-group row">
            <label class="control-label col-sm-4">Catatan Teknisi</label>
            <div class="col-sm-8">
                <textarea class="form-control" rows="5" name="<?=COL_NOTE_PSIKOLOG?>" required="true"><?= $edit ? $data[COL_NOTE_PSIKOLOG] : ""?></textarea>
            </div>
        </div>
        <?php
      }
      ?>

    </div>
</div>
<?php
if($isajax) {
  ?>
  <?=form_close()?>
  <?php
}
?>
<script>
$(document).ready(function() {
  $('[name=<?=COL_ID_PSIKOLOG?>]').change(function() {
    $.post('<?=site_url("ajax/get-opt-service")?>', {UserName: $(this).val()}, function(dat) {
      $('[name=<?=COL_ID_TYPE?>]').empty();
      $('[name=<?=COL_ID_TYPE?>]').select2({
        width: 'resolve',
        theme: 'bootstrap4',
        placeholder: '-- Pilih Jenis Layanan / Jasa --',
        data: JSON.parse(dat)
      }).trigger('change');
    });
  }).trigger('change');
});
</script>
