<?php
$user = GetLoggedUser();
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('layouts/backend-header');
} else {
    $this->load->view('layouts/frontend-header');
}
?>
<div class="content-header">
    <div class="<?=$user[COL_ROLEID]==ROLEADMIN?'container-fluid':'container'?>">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=site_url('schedule/index')?>"> <?=$title?></a></li>
                    <li class="breadcrumb-item active"><?=$edit?'Edit':'Tambah'?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="<?=$user[COL_ROLEID]==ROLEADMIN?'container-fluid':'container'?>">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-primary">
                  <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'schedule-form','class'=>'form-horizontal'))?>
                  <div class="card-body">
                      <div style="display: none" class="alert alert-danger errorBox">
                          <i class="fa fa-ban"></i> Error :
                          <span class="errorMsg"></span>
                      </div>
                      <?php
                      if($this->input->get('error') == 1){
                          ?>
                          <div class="alert alert-danger alert-dismissible">
                              <i class="fa fa-ban"></i>
                              <span class="">Data gagal disimpan, silahkan coba kembali</span>
                          </div>
                      <?php
                      }
                      if(validation_errors()){
                          ?>
                          <div class="alert alert-danger alert-dismissible">
                              <i class="fa fa-ban"></i>
                              <?=validation_errors()?>
                          </div>
                      <?php
                      }
                      if(!empty($upload_errors)) {
                          ?>
                          <div class="alert alert-danger alert-dismissible">
                              <i class="fa fa-ban"></i>
                              <?=$upload_errors?>
                          </div>
                      <?php
                      }
                      ?>
                      <?php $this->load->view('schedule/form_partial') ?>
                  </div>
                  <div class="card-footer">
                      <div class="row">
                          <div class="col-md-12">
                              <a href="<?=site_url('schedule/index')?>" class="btn btn-default">BACK</a>
                              <button type="submit" class="btn btn-primary">SUBMIT</button>
                          </div>
                      </div>
                  </div>
                  <?=form_close()?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php $this->load->view('layouts/_js') ?>
<?php
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('layouts/backend-footer');
} else {
    $this->load->view('layouts/frontend-footer');
}
?>
