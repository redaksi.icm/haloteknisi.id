<?php
$this->load->view('layouts/frontend-header');
?>
<style>
.rating {
  display: inline-block;
  position: relative;
  font-size: 20pt;
}

.rating label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.rating label:last-child {
  position: static;
}

.rating label:nth-child(1) {
  z-index: 5;
}

.rating label:nth-child(2) {
  z-index: 4;
}

.rating label:nth-child(3) {
  z-index: 3;
}

.rating label:nth-child(4) {
  z-index: 2;
}

.rating label:nth-child(5) {
  z-index: 1;
}

.rating label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.rating label .icon {
  float: left;
  color: transparent;
}

.rating label:last-child .icon {
  color: #000;
}

.rating:not(:hover) label input:checked ~ .icon,
.rating:hover label:hover input ~ .icon {
  color: #ffc107;
}

.rating label input:focus:not(:checked) ~ .icon:last-child {
  color: #ffc107;
  text-shadow: 0 0 5px #ffc107;
}
</style>
<?php
$user = GetLoggedUser();
$rate = $this->db
->select('AVG(Rate) as Rate')
->where(COL_TYPE, 'RATE')
->where(COL_ID_APPOINTMENT, $data[COL_ID_APPOINTMENT])
->get(TBL_T_APPOINTMENT_FEEDBACK)
->row_array();
?>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="font-weight-light">
          <?= $title ?>&nbsp;
          <small>
            <?php
            if(!empty($rate) && $rate[COL_RATE] != 0) {
              $nrate = toNum($rate[COL_RATE]);
              $rate = '';
              $floating = ($nrate - floor($nrate)) != 0;
              for($i=0; $i<floor($nrate); $i++) {
                $rate .= '<i class="text-warning fas fa-star"></i>';
              }
              if($floating) {
                $rate .= '<i class="text-warning fas fa-star-half-alt"></i>';
              }
              echo $rate;
            } else {
              if($user[COL_USERNAME] == $data[COL_ID_PATIENT]) {
                ?>
                <a href="<?=site_url('schedule/comment-add/'.$data[COL_ID_APPOINTMENT].'/1')?>" class="modal-popup-rating">Beri rating</a>
                <?php
              }
            }
            ?>
          </small>
        </h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fad fa-home"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('schedule/index')?>">Permintaan</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-primary">
          <div class="card-header">
            <h5 class="card-title m-0"><i class="far fa-info-circle"></i>&nbsp;DETAIL</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <td style="width: 180px">No. Permintaan</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=str_pad($data[COL_ID_APPOINTMENT], 5, '0', STR_PAD_LEFT)?></td>
                </tr>
                <tr>
                  <td style="width: 150px">Tanggal</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=date('d-m-Y', strtotime($data[COL_DATE_SCHEDULE]))?></td>
                </tr>
                <tr>
                  <td style="width: 150px">Layanan / Jasa</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=$data[COL_NM_TYPE]?></td>
                </tr>
                <tr>
                  <td style="width: 150px">Status</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><span class="badge" style="background-color: <?=$data[COL_NM_COLOR]?>; color: #000"><?=strtoupper($data[COL_NM_STATUS])?></span></td>
                </tr>
                <tr>
                  <td style="width: 150px">Penyedia</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=anchor(site_url('user/detail/'.$data["ID_Provider"]), $data["NM_Provider"], array('target'=>'_blank'))?></td>
                </tr>
                <tr>
                  <td style="width: 150px">Konsumen</td><td style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=anchor(site_url('user/detail/'.$data["ID_Member"]), $data["NM_Member"], array('target'=>'_blank'))?></td>
                </tr>
                <tr>
                  <td style="width: 180px">Keluhan / Catatan</td><td style="width: 10px">:</td>
                  <td><?=$data[COL_NOTE_PATIENT]?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div id="card-messages" class="card direct-chat direct-chat-info card-info">
          <div class="card-header">
            <h5 class="card-title m-0"><i class="far fa-comment-alt-lines"></i>&nbsp;INTERAKSI</h5>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('schedule/comments/'.$data[COL_ID_APPOINTMENT])?>"><i class="fas fa-sync-alt"></i></button>
            </div>
          </div>
          <div class="card-body">

          </div>
          <?php
          if(!($data[COL_ID_STATUS]==STATUS_SELESAI || $data[COL_ID_STATUS]==STATUS_BATAL)) {
            ?>
            <div class="card-footer">
              <form id="comment-form" action="<?=site_url('schedule/comment-add/'.$data[COL_ID_APPOINTMENT])?>" method="post">
                <div class="input-group">
                  <input type="text" name="<?=COL_COMMENT?>" placeholder="Ketik pesan / komentar ..." class="form-control">
                  <span class="input-group-append">
                    <button id="btn-add-comment" type="submit" class="btn btn-outline-info"><i class="fad fa-paper-plane"></i>&nbsp;KIRIM</button>
                  </span>
                </div>
              </form>
            </div>
            <?php
          }
           ?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-rating" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Beri Rating</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times fa-sm"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor-rating" method="post" action="#">
          <div class="form-group row">
            <label class="control-label col-sm-4">Rating</label>
            <div class="col-sm-8">
              <div class="rating">
                <label>
                  <input type="radio" name="<?=COL_RATE?>" value="1" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_RATE?>" value="2" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_RATE?>" value="3" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_RATE?>" value="4" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_RATE?>" value="5" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-4">Feedback</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3" name="<?=COL_COMMENT?>" placeholder="Ketikkan komentar anda..."></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Cancel</button>
        <button type="button" class="btn btn-primary btn-ok"><i class="fas fa-pencil"></i>&nbsp;Submit</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function scrollMessages() {
  var el = $('.direct-chat-messages');
  var msgHeightScroll = el[0].scrollHeight;
  var msgHeight = el.height();
  var scrollVal = msgHeightScroll - msgHeight;
  el.stop().animate({scrollTop:scrollVal}, 500, 'swing', function() {

  });
}
$(document).ready(function() {
  $('.btn-tool[data-card-widget=card-refresh]', $("#card-messages")).click();
  $('#btn-add-comment').click(function() {
    var dis = $(this);
    var msg = $('[name=<?=COL_COMMENT?>]', $("#comment-form")).val();
    if(!msg) {
      toastr.error('Komentar kosong.');
      return false;
    }
    dis.html('Loading...').attr('disabled', true);
    $('#comment-form').ajaxSubmit({
        dataType: 'json',
        success : function(data){
          if(data.error==0){
            toastr.success('Komentar ditambahkan.');
            $('.btn-tool[data-card-widget=card-refresh]', $("#card-messages")).click();
            setTimeout(function() {
              scrollMessages();
            }, 1000);
          } else{
            toastr.error(data.error);
          }
        },
        error: function() {
          toastr.error('Server Problem');
        },
        complete: function() {
          dis.html('<i class="fad fa-paper-plane"></i>&nbsp;KIRIM').attr("disabled", false);
          $('[name=<?=COL_COMMENT?>]', $("#comment-form")).val('');
        }
    });
    return false;
  });

  $('.modal-popup-rating').click(function(){
    var a = $(this);
    var editor = $("#modal-rating");

    editor.modal("show");
    $(".btn-ok", editor).unbind('click').click(function() {
      var dis = $(this);
      dis.html("Loading...").attr("disabled", true);
      $('#form-editor-rating').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            window.location.reload();
          }else{
            toastr.error(data.error);
          }
        },
        error: function() {
          toastr.error('Server Problem');
        },
        complete: function() {
          dis.html('<i class="fas fa-pencil"></i>&nbsp;Submit').attr("disabled", false);
          editor.modal("hide");
        }
      });
    });
    return false;
  });
});
</script>
<?php
$this->load->view('layouts/_js');
$this->load->view('layouts/frontend-footer');
?>
