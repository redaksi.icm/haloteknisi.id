<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 11:17
 */
$ruser = GetLoggedUser();
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('header');
} else {
    $this->load->view('header-front');
}
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('order/index')?>"> <?=$title?></a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'order-form','class'=>'form-horizontal'))?>
                        <div class="card-body">
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i> Error :
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('error') == 1){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <span class="">Data gagal disimpan, silahkan coba kembali</span>
                                </div>
                            <?php
                            }
                            if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=validation_errors()?>
                                </div>
                            <?php
                            }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row" <?=$ruser[COL_ROLEID]==ROLECUSTOMER?'style="display: none"':null?>>
                                        <label class="control-label col-sm-4">Konsumen</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_CUSTOMER?>" class="form-control" required>
                                                <option value="">-- Pilih Konsumen --</option>
                                                <?=GetCombobox("SELECT * FROM mcustomer ORDER BY NM_Customer", COL_ID_CUSTOMER, COL_NM_CUSTOMER, ($edit ? $data[COL_ID_CUSTOMER] : ($ruser[COL_ROLEID]==ROLECUSTOMER?$ruser[COL_COMPANYID]:null)))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Tipe Service</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_SERVICE?>" class="form-control" required>
                                                <option value="">-- Pilih Service --</option>
                                                <?=GetCombobox("SELECT mservice.ID_Service, mservice.NM_Service, CONCAT(Duration, ' m') as Duration FROM mservice ORDER BY NM_Service", COL_ID_SERVICE, array(COL_DURATION, COL_NM_SERVICE), ($edit ? $data[COL_ID_SERVICE] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Kendaraan</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_VEHICLE?>" class="form-control" required>
                                                <?=GetCombobox("SELECT * FROM mvehicle left join mvehicletype t on t.ID_Type = mvehicle.ID_Type where ID_Customer = ".(!empty($data)?$data[COL_ID_CUSTOMER]:($ruser[COL_ROLEID]==ROLECUSTOMER?$ruser[COL_COMPANYID]:'-1'))." ORDER BY t.NM_Type", COL_ID_VEHICLE, array(COL_NO_PLAT, COL_NM_TYPE), ($edit ? $data[COL_ID_VEHICLE] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Mekanik</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_MECHANIC?>" class="form-control" required>
                                                <option value="">-- Pilih Mekanik --</option>
                                                <?=GetCombobox("SELECT * FROM mmechanic ORDER BY NM_Mechanic", COL_ID_MECHANIC, COL_NM_MECHANIC, ($edit ? $data[COL_ID_MECHANIC] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                    if($ruser[COL_ROLEID] == ROLEADMIN) {
                                        ?>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4">Status</label>
                                            <div class="col-sm-8">
                                                <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                                                    <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS, ($edit ? $data[COL_ID_STATUS] : null))?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Tanggal</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control datepicker" name="<?=COL_DATE?>" value="<?= $edit ? $data[COL_DATE] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">No. Telp / HP / WA</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="<?=COL_NO_TELP?>" value="<?= $edit ? $data[COL_NO_TELP] : ""?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Catatan Konsumen</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" rows="4" name="<?=COL_RE_CUSTOMER?>" required="true"><?= $edit ? $data[COL_RE_CUSTOMER] : ""?></textarea>
                                        </div>
                                    </div>
                                    <?php
                                    if($ruser[COL_ROLEID] == ROLEADMIN) {
                                        ?>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4">Catatan Mekanik</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" rows="4" name="<?=COL_RE_MECHANIC?>" required="true"><?= $edit ? $data[COL_RE_MECHANIC] : ""?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4">Catatan Akhir</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" rows="4" name="<?=COL_REMARKS?>" required="true"><?= $edit ? $data[COL_REMARKS] : ""?></textarea>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?=site_url('order/index')?>" class="btn btn-default">BACK</a>
                                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[name=ID_Customer]").change(function() {
                var id_cust = $("[name=ID_Customer]").val();
                $("[name=ID_Vehicle]").load('<?=site_url("master/get-opt-vehicle")?>', {ID_Customer: id_cust}, function() {

                });
            });
        });
    </script>
<?php
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('footer');
} else {
    $this->load->view('footer-front');
}
?>