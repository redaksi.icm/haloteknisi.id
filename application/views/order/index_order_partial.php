<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 31/01/2020
 * Time: 17:15
 */
?>
<table class="table table-striped projects">
    <thead>
    <tr>
        <th>Antrian</th>
        <th>Jenis Service</th>
        <th>Status</th>
        <th>Mekanik</th>
        <th>Selesai</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(count($res) > 0) {
        foreach ($res as $d) {
            $est = '-';
            if ($d[COL_ID_STATUS] == STATUS_ORDER_PROSES) {
                $est = date("H:i", strtotime('+' . $d[COL_DURATION] . ' minutes', strtotime($d['START'])));
            }
            ?>
            <tr>
                <td>
                    No. <b><?= str_pad($d[COL_ID_ORDER], 5, '0', STR_PAD_LEFT) ?></b><br/>
                    a.n <?= $d[COL_NM_CUSTOMER] ?>
                </td>
                <td><?= $d[COL_NM_SERVICE] ?></td>
                <td><span class="badge"
                          style="background-color: <?= $d[COL_NM_LABELCOLOR] ?>; color: #fff"><?= strtoupper($d[COL_NM_STATUS]) ?></span>
                </td>
                <td><?= $d[COL_NM_MECHANIC] ?></td>
                <td><?= $est ?></td>
            </tr>
        <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="5" class="font-italic">Tidak ada data tersedia.</td>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>