<?php $this->load->view('layouts/frontend-header') ?>
<?php
$ruser = GetLoggedUser();
 ?>
<div class="content-header">
    <div class="container">
        <!--<div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>-->
    </div>
</div>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-default card-filter">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Temukan Penyedia Jasa di sekitar anda!</h5>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body" style="display: block">
            <?=form_open_multipart(site_url('home/index'),array('role'=>'form','id'=>'search-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Jenis Jasa</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fad fa-tools"></i></span>
                    </div>
                    <select name="<?=COL_ID_TYPE?>" class="form-control" required>
                        <option value="">-- Pilih Jenis Jasa --</option>
                        <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_ID_TYPE, COL_NM_TYPE, (!empty($filter)?$filter[COL_ID_TYPE]:null))?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Provinsi</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fad fa-map"></i></span>
                    </div>
                    <select name="<?=COL_NM_PROVINSI?>" class="form-control no-select2">
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Kabupaten / Kota</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fad fa-map-marker-alt"></i></span>
                    </div>
                    <select name="<?=COL_NM_KOTA?>" class="form-control no-select2">
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <?=form_close()?>
          </div>
          <div class="card-footer text-right">
            <button type="button" id="btn-filter" class="btn btn-sm btn-outline-secondary"><i class="fad fa-search"></i>&nbsp;&nbsp;CARI</button>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
              <div class="row d-flex align-items-stretch">
                <?php
                if(count($rpsikolog) > 0) {
                  foreach($rpsikolog as $p) {
                    ?>
                    <div class="col-12 col-sm-12 col-md-6 d-flex align-items-stretch">
                      <div class="card bg-light">
                        <!--<div class="card-header text-muted border-bottom-0">
                          <?=$p[COL_NM_FULLNAME]?>
                        </div>-->
                        <div class="card-body pb-0">
                          <div class="row">
                            <div class="col-8">
                              <h2 class="lead"><b><?=$p[COL_NM_FULLNAME]?></b></h2>
                              <p>
                                <?php
                                $rrate = $this->db
                                ->select('AVG(t_appointment_feedback.Rate) as Rate')
                                ->join(TBL_T_APPOINTMENT,TBL_T_APPOINTMENT.'.'.COL_ID_APPOINTMENT." = ".TBL_T_APPOINTMENT_FEEDBACK.".".COL_ID_APPOINTMENT,"left")
                                ->where(COL_TYPE, 'RATE')
                                ->where(TBL_T_APPOINTMENT.".".COL_ID_PSIKOLOG, $p[COL_USERNAME])
                                ->get(TBL_T_APPOINTMENT_FEEDBACK)
                                ->row_array();

                                $nrate = toNum($rrate[COL_RATE]);
                                $floating = ($nrate - floor($nrate)) != 0;
                                $rate = '';
                                if($nrate==0) {
                                  $rate = '<small class="font-italic">Belum ada rating.</small>';
                                }

                                for($i=0; $i<floor($nrate); $i++) {
                                  $rate .= '<i class="text-warning fas fa-star"></i>';
                                }
                                if($floating) {
                                  $rate .= '<i class="text-warning fas fa-star-half-alt"></i>';
                                }
                                echo $rate;
                                 ?>
                               </p>
                               <ul class="col-12 ml-3 mb-0 fa-ul text-muted">
                                 <li class="small mb-1">
                                   <span class="fa-li">
                                     <i class="fad fa-lg fa-phone-square"></i>
                                   </span> <?=$p[COL_NM_PHONENO]?>
                                 </li>
                                 <li class="small mb-1">
                                   <span class="fa-li">
                                     <i class="fad fa-lg fa-map-marker-alt"></i>
                                   </span> <?=$p[COL_NM_ADDRESS].', '.$p[COL_NM_KOTA].', '.$p[COL_NM_PROVINSI]?>
                                 </li>
                                 <li class="small mb-1">
                                   <span class="fa-li">
                                     <i class="fad fa-lg fa-quote-left"></i>
                                   </span>
                                   <p class="text-muted text-sm">
                                     <?=strlen($p[COL_NM_ABOUT]) > 100 ? substr($p[COL_NM_ABOUT], 0, 100) . "..." : $p[COL_NM_ABOUT] ?>
                                   </p>
                                 </li>
                               </ul>
                            </div>
                            <div class="col-4 pt-2 pb-2 text-center">
                              <?php
                              $imgurl = !empty($p[COL_NM_IMAGELOCATION]) ? MY_UPLOADURL.$p[COL_NM_IMAGELOCATION] : MY_IMAGEURL.$this->setting_web_logo;
                               ?>
                              <img src="<?=$imgurl?>" alt="" class="img-fluid elevation-2">
                            </div>
                          </div>
                        </div>
                        <div class="card-footer">
                          <div class="text-right">
                             &nbsp;
                            <a href="<?=site_url('user/detail/'.$p[COL_UNIQ])?>" class="btn btn-sm btn-outline-primary">
                              <i class="fad fa-info-circle"></i>
                            </a>
                            <?php
                            if(IsLogin() && $ruser[COL_ROLEID] != ROLEPROVIDER) {
                              ?>
                              <a href="<?=site_url('schedule/add/1/'.$p[COL_UNIQ])?>" data-psikolog="<?=$p[COL_USERNAME]?>" class="btn btn-sm btn-outline-success btn-popup">
                                <i class="fad fa-tools"></i> BUAT PERMINTAAN
                              </a>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                } else {
                  ?>
                  <p>Tidak ada data untuk ditampilkan.</p>
                  <?php
                }
                 ?>
              </div>
            </div>
            <div class="card-footer">

            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-weight-light">Permintaan Jasa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-close"></i></span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal"><i class="far fa-times"></i>&nbsp;CANCEL</button>
                <button type="button" class="btn btn-sm btn-outline-success btn-ok"><i class="far fa-check"></i>&nbsp;SUBMIT</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $this->load->view('layouts/_js') ?>
<script type="text/javascript">
$(document).ready(function(){
  $('.btn-popup').click(function(){
      var url = $(this).attr('href');
      var modal = $("#modal-popup");
      var modalBody = $(".modal-body", modal);

      modal.on('shown.bs.modal', function (e) {
        $('select', modalBody).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
        $('.datepicker').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          minYear: 1900,
          maxYear: parseInt(moment().format('YYYY'),10),
          locale: {
              format: 'Y-MM-DD'
          }
        });
        $(".datepicker").attr("autocomplete", "off");
      });
      modal.on('hidden.bs.modal', function (e) {
        $('.btn-ok', modalBody).html("SUBMIT").attr("disabled", false);
      });
      modal.modal("show");

      modalBody.load(url, function() {

      });
      $(".btn-ok", modal).unbind('click').click(function() {
        var dis = $(this);
        dis.html("Loading...").attr("disabled", true);
        $('form', modalBody).ajaxSubmit({
            dataType: 'json',
            url : url,
            success : function(data){
              if(data.error != 0){
                  toastr.error(data.error);
              }else{
                  toastr.success("Permintaan berhasil ditambahkan.");
              }
            },
            error: function(data) {
              toastr.error('Server error.');
            },
            complete: function() {
              dis.html('<i class="fad fa-check"></i>&nbsp;SUBMIT').attr("disabled", false);
              setTimeout(function() {
                location.reload();
              }, 1000);
            }
        });
      });
      return false;
  });

  $.get('https://wilayah-api.herokuapp.com/provinsi', function(data) {
    var dataProvinsi = $.merge([{id:'', text:''}], $.map(data, function(n,i){
      return {id: n.name, text: n.name, selected: n.name=='<?=$filter[COL_NM_PROVINSI]?>'}
    }));

    $('[name=<?=COL_NM_PROVINSI?>]').select2({
      width: 'resolve',
      theme: 'bootstrap4',
      allowClear: true,
      placeholder: 'Pilih Provinsi',
      data: dataProvinsi
    }).trigger('change');
  });


  $('[name=<?=COL_NM_PROVINSI?>]').change(function() {
    var dis = $(this);
    var url = 'https://wilayah-api.herokuapp.com/provinsi?name='+dis.val();

    $.get(url, function(data) {
      var provId = 0;
      if(data && Array.isArray(data)) {
        provId = data[0].id;
      }

      $.get('https://wilayah-api.herokuapp.com/kabupaten?province_id='+provId, function(kot) {
        var dataKota = $.merge([{id:'', text:''}], $.map(kot, function(n,i){
          return {id: n.name, text: n.name, selected: n.name=='<?=$filter[COL_NM_KOTA]?>'}
        }));

        $('[name=<?=COL_NM_KOTA?>]').empty();
        $('[name=<?=COL_NM_KOTA?>]').select2({
          width: 'resolve',
          theme: 'bootstrap4',
          allowClear: true,
          placeholder: 'Pilih Kabupaten / Kota',
          data: dataKota
        }).trigger('change');
      });
    });
  });

  $('#btn-filter').click(function() {
    $('#search-form').submit();
  });
});

</script>
<?php $this->load->view('layouts/frontend-footer') ?>
