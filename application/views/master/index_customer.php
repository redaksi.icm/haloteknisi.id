<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/1/2019
 * Time: 10:46 PM
 */

$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_ID_CUSTOMER] . '" />',
        anchor('master/customer-edit/'.$d[COL_ID_CUSTOMER],$d[COL_NM_CUSTOMER],array('class' => 'modal-popup-edit')),
        $d[COL_NM_ADDRESS],
        $d[COL_NM_PHONENO],
        $d[COL_NM_EMAIL],
        /*$d[COL_CREATEDBY],
        date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))*/
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p>
                        <?=anchor('master/customer-delete','<i class="fa fa-trash-o"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                        <?=anchor('master/customer-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'btn btn-primary btn-sm'))?>
                    </p>
                    <div class="card card-default">
                        <div class="card-body">
                            <form id="dataform" method="post" action="#">
                                <table id="datalist" class="table table-bordered table-hover">

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '44vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
                    {"sTitle": "Name"},
                    {"sTitle": "Address"},
                    {"sTitle": "Contact No."},
                    {"sTitle": "Email"},
                    /*{"sTitle": "Created By"},
                    {"sTitle": "Created On"}*/
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')?>