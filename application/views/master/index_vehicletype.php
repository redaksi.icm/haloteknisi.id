<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 28/01/2020
 * Time: 20:24
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_ID_TYPE] . '" />',
        anchor('master/vehicletype-edit/'.$d[COL_ID_TYPE],$d[COL_NM_TYPE],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NM_TYPE])),
        $d[COL_CREATEDBY],
        date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p>
                        <?=anchor('master/vehicletype-delete','<i class="fa fa-trash-o"></i> DELETE',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                        <?=anchor('master/vehicletype-add','<i class="fa fa-plus"></i> CREATE',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
                    </p>
                    <div class="card card-default">
                        <div class="card-body">
                            <form id="dataform" method="post" action="#">
                                <table id="datalist" class="table table-bordered table-hover">

                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Editor</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="text-danger error-message"></p>
                                    <form id="form-editor" method="post" action="#">
                                        <div class="form-group">
                                            <label>Tipe Kendaraan</label>
                                            <input type="text" class="form-control" name="<?=COL_NM_TYPE?>" required>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-primary btn-flat btn-ok">Simpan</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '44vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "columnDefs": [
                    { className: "text-center", "targets": [ 2,3 ] }
                ],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
                    {"sTitle": "Tipe Kendaraan"},
                    {"sTitle": "Created By"},
                    {"sTitle": "Created On"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });

            $('.modal-popup, .modal-popup-edit').click(function(){
                var a = $(this);
                var name = $(this).data('name');
                var editor = $("#modal-editor");

                $('[name=<?=COL_NM_TYPE?>]', editor).val(name);
                editor.modal("show");
                $(".btn-ok", editor).unbind('click').click(function() {
                    $(this).html("Loading...").attr("disabled", true);
                    $('#form-editor').ajaxSubmit({
                        dataType: 'json',
                        url : a.attr('href'),
                        success : function(data){
                            if(data.error==0){
                                window.location.reload();
                            }else{
                                $(".error-message", editor).html(data.error);
                            }
                        }
                    });
                });
                return false;
            });
        });
    </script>

<?php $this->load->view('footer')?>