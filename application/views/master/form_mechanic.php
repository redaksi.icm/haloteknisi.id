<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 29/01/2020
 * Time: 00:06
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('master/mechanic-index')?>"> <?=$title?></a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
                        <div class="card-body">
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i> Error :
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('error') == 1){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <span class="">Data gagal disimpan, silahkan coba kembali</span>
                                </div>
                            <?php
                            }
                            if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=validation_errors()?>
                                </div>
                            <?php
                            }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Nama Mekanik</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="<?=COL_NM_MECHANIC?>" value="<?= $edit ? $data[COL_NM_MECHANIC] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Tahun Bergabung</label>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control" name="<?=COL_TH_BERGABUNG?>" value="<?= $edit ? $data[COL_TH_BERGABUNG] : date('Y')?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">No. Telepon / HP / WAA</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="<?=COL_NO_TELP?>" value="<?= $edit ? $data[COL_NO_TELP] : ""?>" required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?=site_url('master/mechanic-index')?>" class="btn btn-default">BACK</a>
                                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>