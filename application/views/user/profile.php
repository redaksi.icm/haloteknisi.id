<?php $this->load->view('layouts/frontend-header') ?>
<?php
$displayname = $data ? $data[COL_NM_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($data) {
    $displaypicture = $data[COL_NM_IMAGELOCATION] ? MY_UPLOADURL.$data[COL_NM_IMAGELOCATION] : MY_IMAGEURL.'user.jpg';
}
$rarea = $this->db
->where(COL_USERNAME, $data[COL_USERNAME])
->order_by(COL_NM_PROVINSI, 'asc')
->order_by(COL_NM_KOTA, 'asc')
->get(TBL_SERVICE_AREA)
->result_array();

$rservice = $this->db
->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_ID_TYPE." = ".TBL_SERVICE.".".COL_ID_TYPE,"left")
->where(COL_USERNAME, $data[COL_USERNAME])
->order_by(COL_NM_TYPE, 'asc')
->get(TBL_SERVICE)
->result_array();
$arrService = array();
foreach($rservice as $s) {
  $arrService[] = $s[COL_ID_TYPE];
}
?>
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 font-weight-light"><?= $title ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="card card-secondary card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="<?=$displaypicture?>" alt="Profile">
            </div>
            <h3 class="profile-username text-center"><?=$data[COL_NM_FULLNAME]?></h3>
            <?php
            if($data[COL_ROLEID]==ROLEPROVIDER) {
              $star = rand(0,5);
              $star_half = $star == 0 ? 1 : ($star == 5 ? 0 : rand(0,1));
              echo '<p class="text-muted text-center">';
              for($i=0; $i<$star; $i++) {
                ?>
                <i class="fad fa-star text-orange"></i>
                <?php
              }
              for($i=0; $i<$star_half; $i++) {
                ?>
                <i class="fad fa-star-half text-orange"></i>
                <?php
              }
              echo '</p>';
            } else {
              ?>
              <p class="text-muted text-center"><?=$data[COL_ROLENAME]?></p>
              <?php
            }
            ?>
            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b><i class="fad fa-calendar"></i>&nbsp;&nbsp;Bergabung</b> <a class="float-right"><?=date('d-m-Y', strtotime($data[COL_DATE_REGISTERED]))?></a>
              </li>
              <li class="list-group-item">
                <b><i class="fad fa-at"></i>&nbsp;&nbsp;Email</b> <a class="float-right"><?=$data[COL_NM_EMAIL]?></a>
              </li>
              <li class="list-group-item">
                <b><i class="fad fa-phone"></i>&nbsp;&nbsp;No. Telepon</b> <a class="float-right"><?=$data[COL_NM_PHONENO]?></a>
              </li>
              <!--<li class="list-group-item">
                <b><i class="fad fa-map"></i>&nbsp;&nbsp;Provinsi</b> <a class="float-right"><?=$data[COL_NM_PROVINSI]?></a>
              </li>
              <li class="list-group-item">
                <b><i class="fad fa-map-marker-alt"></i>&nbsp;&nbsp;Kab / Kota</b> <a class="float-right"><?=$data[COL_NM_KOTA]?></a>
              </li>-->
            </ul>
            <b><i class="fad fa-map-signs"></i>&nbsp;&nbsp;Alamat</b>
            <p><?=$data[COL_NM_ADDRESS].", ".$data[COL_NM_KOTA].", ".$data[COL_NM_PROVINSI]?></p>
            <!--<b><i class="fad fa-location"></i>&nbsp;&nbsp;Jangkauan</b>
            <ul>
              <?php
              foreach($rarea as $a) {
                ?>
                <li><?=$a[COL_NM_PROVINSI].' - '.$a[COL_NM_KOTA]?></li>
                <?php
              }
              ?>
            </ul>-->
            <!--<b><i class="fas fa-quote-left"></i>&nbsp;&nbsp;About</b>
            <p class="pl-3"><?=$data[COL_NM_ABOUT]?></p>-->
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card card-secondary card-outline">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <?php
              if(empty($view)) {
                ?>
                <li class="nav-item"><a class="nav-link active" href="#editprofile" data-toggle="tab">Profil</a></li>
                <?php
                if($data[COL_ROLEID] == ROLEPROVIDER) {
                  ?>
                  <li class="nav-item"><a class="nav-link" href="#editservice" data-toggle="tab">Layanan / Jasa</a></li>
                  <?php
                }
                ?>
                <li class="nav-item"><a class="nav-link" href="#editpassword" data-toggle="tab">Password</a></li>
                <?php
              } else {
                ?>
                <li class="nav-item"><a class="nav-link active" href="#detail" data-toggle="tab">Detail</a></li>
                <li class="nav-item"><a class="nav-link" href="#feedback" data-toggle="tab">Testimoni</a></li>
                <li class="nav-item"><a class="nav-link" href="#history" data-toggle="tab">Riwayat</a></li>
                <?php
              }
              ?>
            </ul>

          </div>
          <div class="card-body">
            <div class="tab-content">
              <?php
              if(empty($view)) {
                ?>
                <div id="editprofile" class="tab-pane active">
                  <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'profile-form','class'=>'form-horizontal'))?>
                  <div class="form-group row">
                    <label class="control-label col-sm-4" for="avatarFile">Avatar (opsional)</label>
                    <div class="col-sm-4">
                      <div class="input-group">
                        <div class="custom-file">
                          <input id="avatarFile" type="file" class="custom-file-input" name="avatar" accept="image/*">
                          <label class="custom-file-label" for="avatarFile">Pilih file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Nama Lengkap</label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control" name="<?=COL_NM_FULLNAME?>" placeholder="John Doe" value="<?=$data[COL_NM_FULLNAME]?>" required />
                      </div>
                  </div>
                  <!--<div class="form-group row">
                      <label class="control-label col-sm-4">Jenis Kelamin</label>
                      <div class="col-sm-2">
                        <div class="form-check">
                          <input id="RadioGenderLK" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Pria" <?=empty($data[COL_NM_GENDER])?"checked":($data[COL_NM_GENDER]=="Pria"?"checked":"")?>>
                          <label class="form-check-label" for="RadioGenderLK">Pria</label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-check">
                          <input id="RadioGenderPR" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Wanita" <?=$data[COL_NM_GENDER]=="Wanita"?"checked":""?>>
                          <label class="form-check-label" for="RadioGenderPR">Wanita</label>
                        </div>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Tanggal Lahir</label>
                      <div class="col-sm-2">
                          <input type="text" class="form-control datepicker" name="<?=COL_DATE_BIRTH?>" placeholder="dd-mm-yyyy" value="<?=$data[COL_DATE_BIRTH]?>" required />
                      </div>
                  </div>-->
                  <div class="form-group row">
                      <label class="control-label col-sm-4">No. Telp / HP / WA</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" placeholder="081x-xxxx-xxxx" value="<?=$data[COL_NM_PHONENO]?>" required />
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Provinsi</label>
                      <div class="col-sm-8">
                          <select name="<?=COL_NM_PROVINSI?>" class="form-control no-select2" style="width: 100%" required>
                          </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Kabupaten / Kota</label>
                      <div class="col-sm-8">
                          <select name="<?=COL_NM_KOTA?>" class="form-control no-select2" style="width: 100%" required>
                          </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Alamat</label>
                      <div class="col-sm-8">
                          <textarea class="form-control" rows="3" name="<?=COL_NM_ADDRESS?>" required="true"><?=$data[COL_NM_ADDRESS]?></textarea>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">About</label>
                      <div class="col-sm-8">
                          <textarea class="form-control" rows="5" name="<?=COL_NM_ABOUT?>" required="true"><?=$data[COL_NM_ABOUT]?></textarea>
                      </div>
                  </div>
                  <div class="form-group row mb-0">
                    <div class="col-sm-12 text-center">
                      <a href="<?=site_url()?>" class="btn btn-default"><i class="fas fa-arrow-left"></i>&nbsp;KEMBALI</a>
                      <button type="button" id="btn-profile-submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div>

                <div id="editservice" class="tab-pane">
                  <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'service-form','class'=>'form-horizontal'))?>
                  <div class="form-group row">
                      <label class="control-label col-sm-2">Layanan / Jasa</label>
                      <div class="col-sm-10">
                          <select name="ID_Type[]" class="form-control no-select2" style="width: 100%" multiple>
                            <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_ID_TYPE, COL_NM_TYPE, $arrService)?>
                          </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-2">Area Jangkauan</label>
                      <div class="col-sm-10">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Provinsi</th>
                              <th>Kabupaten / Kota</th>
                              <th class="text-center" style="width: 10px">#</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            foreach($rarea as $a) {
                              ?>
                              <tr>
                                <td><?=$a[COL_NM_PROVINSI]?></td>
                                <td><?=$a[COL_NM_KOTA]?></td>
                                <td>
                                  <input type="hidden" name="NM_Provinsi[]" value="<?=$a[COL_NM_PROVINSI]?>" /><input type="hidden" name="NM_Kota[]" value="<?=$a[COL_NM_KOTA]?>" />
                                  <button type="button" class="btn btn-default btn-del-area"><i class="fas fa-minus"></i></button>
                                </td>
                              </tr>
                              <?php
                            }
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>
                                <select name="Prov" class="form-control no-select2" style="width: 100%"></select>
                              </td>
                              <td>
                                <select name="Kota" class="form-control no-select2" style="width: 100%"></select>
                              </td>
                              <td class="text-center">
                                <button type="button" id="btn-add-area" class="btn btn-default"><i class="fas fa-plus"></i></button>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                  </div>
                  <div class="form-group row mb-0">
                    <div class="col-sm-12 text-center">
                      <a href="<?=site_url()?>" class="btn btn-default"><i class="fas fa-arrow-left"></i>&nbsp;KEMBALI</a>
                      <button type="button" id="btn-service-submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div>

                <div id="editpassword" class="tab-pane">
                  <?=form_open(current_url(),array('role'=>'form','id'=>'password-form','class'=>'form-horizontal'))?>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Password Lama</label>
                      <div class="col-sm-6">
                          <input type="password" class="form-control" name="OldPassword" placeholder="Ketik Password Lama" required />
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Password Baru</label>
                      <div class="col-sm-6">
                          <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Ketik Password Baru" required />
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-4">Konfirmasi Password</label>
                      <div class="col-sm-6">
                          <input type="password" class="form-control" name="KonfirmPassword" placeholder="Ketik Ulang Password" required />
                      </div>
                  </div>
                  <div class="form-group row mb-0">
                    <div class="col-sm-12 text-center">
                      <a href="<?=site_url()?>" class="btn btn-default"><i class="fas fa-arrow-left"></i>&nbsp;KEMBALI</a>
                      <button type="button" id="btn-password-submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div>
                <?php
              } else {
                ?>
                <div id="detail" class="tab-pane active">
                  <div class="row">
                    <div class="col-sm-12">
                      <h6><i class="fad fa-quote-right"></i>&nbsp;&nbsp;Tentang <?=strtoupper($data[COL_NM_FULLNAME])?></h6>
                      <div class="callout callout-info">
                        <p><?=$data[COL_NM_ABOUT]?></p>
                      </div>
                      <hr  />
                      <h6><i class="fad fa-tools"></i>&nbsp;&nbsp;Jenis Layanan / Jasa</h6>
                      <ul class="todo-list ui-sortable" data-widget="todo-list">
                      <?php
                      foreach($rservice as $s) {
                        $star = rand(0,5);
                        $star_half = $star == 0 ? 1 : ($star == 5 ? 0 : rand(0,1));
                        ?>
                        <li>
                          <span class="text font-weight-light"><?=$s[COL_NM_TYPE]?></span>
                          <div class="tools d-inline-block">
                            <?php
                            for($i=0; $i<$star; $i++) {
                              ?>
                              <i class="fad fa-star text-orange"></i>
                              <?php
                            }
                            for($i=0; $i<$star_half; $i++) {
                              ?>
                              <i class="fad fa-star-half text-orange"></i>
                              <?php
                            }
                             ?>
                          </div>
                        </li>
                        <?php
                      }
                      ?>
                    </ul>
                    <hr  />
                    <h6><i class="fad fa-location"></i>&nbsp;&nbsp;Jangkauan</h6>
                    <ul class="todo-list ui-sortable" data-widget="todo-list">
                      <?php
                      foreach($rarea as $a) {
                        ?>
                        <li>
                          <span class="text font-weight-light"><?=$a[COL_NM_PROVINSI].' - '.$a[COL_NM_KOTA]?></span>
                        </li>
                        <?php
                      }
                      ?>
                    </ul>
                  </div>
                </div>
                </div>
                <div id="feedback" class="tab-pane">
                  <div class="post clearfix">
                    <div class="user-block" style="float: none !important">
                      <img class="img-circle img-bordered-sm" src="<?=MY_IMAGEURL.'user.jpg'?>">
                      <span class="username text-primary">Jonathan Burke Jr.</span>
                      <span class="description"><?=date('D, d-m-Y H:i', strtotime("-3 days"))?></span>
                    </div>
                    <p>
                      Lorem ipsum represents a long-held tradition for designers,
                      typographers and the like. Some people hate it and argue for
                      its demise, but others ignore the hate as they create awesome
                      tools to help create filler text for everyone from bacon lovers
                      to Charlie Sheen fans.
                    </p>
                  </div>
                </div>
                <div id="history" class="tab-pane">
                  Riwayat
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('layouts/_js') ?>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-profile-submit').click(function() {
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#profile-form').ajaxSubmit({
        dataType: 'json',
        url : '<?=site_url('user/profile-edit/'.$data[COL_UNIQ])?>',
        success : function(data){
          if(data.error != 0) {
            toastr.error(data.error);
          }else {
            toastr.success("Profil berhasil diubah.");
            setTimeout(function() {
              location.reload();
            }, 1000);
          }
        },
        complete: function() {
          dis.html("SUBMIT").attr("disabled", false);
        }
    });
  });

  $('#btn-password-submit').click(function() {
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#password-form').ajaxSubmit({
        dataType: 'json',
        url : '<?=site_url('user/password-change/'.$data[COL_UNIQ])?>',
        success : function(data){
          if(data.error != 0) {
            toastr.error(data.error);
          }else {
            toastr.success("Password berhasil diubah.");
            setTimeout(function() {
              location.reload();
            }, 1000);
          }
        },
        complete: function() {
          dis.html("SUBMIT").attr("disabled", false);
        }
    });
  });

  $.get('https://wilayah-api.herokuapp.com/provinsi', function(data) {
    var dataProvinsi = $.map(data, function(n,i){
      return {id: n.name, text: n.name, selected: n.name=='<?=$data[COL_NM_PROVINSI]?>'}
    });

    $('select[name=<?=COL_NM_PROVINSI?>],select[name=Prov]').select2({
      width: 'resolve',
      theme: 'bootstrap4',
      placeholder: 'Pilih Provinsi',
      data: dataProvinsi
    }).trigger('change');
  });


  $('select[name=<?=COL_NM_PROVINSI?>],select[name=Prov]').change(function() {
    var dis = $(this);
    var url = 'https://wilayah-api.herokuapp.com/provinsi?name='+dis.val();

    $.get(url, function(data) {
      var provId = 0;
      if(data) {
        provId = data[0].id;
      }

      $.get('https://wilayah-api.herokuapp.com/kabupaten?province_id='+provId, function(kot) {
        var dataKota = $.map(kot, function(n,i){
          return {id: n.name, text: n.name, selected: n.name=='<?=$data[COL_NM_KOTA]?>'}
        });

        $('select[name=<?=COL_NM_KOTA?>],select[name=Kota]').empty();
        $('select[name=<?=COL_NM_KOTA?>],select[name=Kota]').select2({
          width: 'resolve',
          theme: 'bootstrap4',
          placeholder: 'Pilih Kabupaten / Kota',
          data: dataKota
        }).trigger('change');
      });
    });
  });

  $('select[name^=ID_Type]').select2({
    width: 'resolve',
    theme: 'bootstrap4',
  });

  $('.btn-del-area').click(function() {
    $(this).closest('tr').remove();
  });
  $('#btn-add-area').click(function() {
    var tbl = $(this).closest('table');
    var NmProv = $('[name=Prov]', $('tfoot', tbl)).val();
    var NmKota = $('[name=Kota]', $('tfoot', tbl)).val();

    var existProv = $('[name^=<?=COL_NM_PROVINSI?>][value="'+NmProv+'"]', $('tbody', tbl));
    var existKota = $('[name^=<?=COL_NM_KOTA?>][value="'+NmKota+'"]', $('tbody', tbl));
    if(existProv.length==0 || existKota.length==0) {
      var row = '';

      row += '<tr>';
      row += '<td>'+NmProv+'</td><td>'+NmKota+'</td>';
      row += '<td class="text-center"><input type="hidden" name="NM_Provinsi[]" value="'+NmProv+'" /><input type="hidden" name="NM_Kota[]" value="'+NmKota+'" /><button type="button" class="btn btn-sm btn-default btn-del-area"><i class="fas fa-minus"></i></button></td>';
      row += '</tr>';
      row = $(row);
      $('.btn-del-area', row).click(function() {
        $(this).closest('tr').remove();
      });
      $('tbody', tbl).append(row);
    }
  });

  $('#btn-service-submit').click(function() {
    var dis = $(this);
    dis.html("Loading...").attr("disabled", true);
    $('#service-form').ajaxSubmit({
        dataType: 'json',
        url : '<?=site_url('user/service-edit/'.$data[COL_UNIQ])?>',
        success : function(data){
          if(data.error != 0) {
            toastr.error(data.error);
          }else {
            toastr.success("Jenis Layanan / Jasa berhasil diubah.");
            setTimeout(function() {
              location.reload();
            }, 1000);
          }
        },
        complete: function() {
          dis.html("SUBMIT").attr("disabled", false);
        }
    });
  });
});
</script>
<?php $this->load->view('layouts/frontend-footer') ?>
