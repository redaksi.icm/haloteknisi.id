<?php
$user = $ruser = GetLoggedUser();
if($ruser[COL_ROLEID] == ROLEADMIN) {
    $this->load->view('layouts/backend-header');
} else {
    $this->load->view('layouts/frontend-header');
}
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <?=form_open(current_url(),array('role'=>'form','id'=>'changepassword'))?>
                            <?php if(validation_errors()){ ?>
                                <div class="form group alert alert-danger">
                                    <i class="fa fa-ban"></i>
                                    <?= validation_errors() ?>
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('success')){ ?>
                                <div class="form-group alert alert-success">
                                    <i class="fa fa-check"></i>
                                    Ganti password berhasil.
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('error')){ ?>
                                <div class="form-group alert alert-danger">
                                    <i class="fa fa-ban"></i>
                                    Gagal mengganti password, silahkan coba kembali
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('nomatch')){ ?>
                                <div class="form-group alert alert-danger">
                                    <i class="fa fa-ban"></i>
                                    Password lama tidak sesuai
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control" name="OldPassword" placeholder="Old Password" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-key"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="New Password" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-key"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control" name="RepeatPassword" placeholder="Repeat Password" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-key"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('layouts/_js') ?>
    <script type="text/javascript">
        $("#deviceForm").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').html(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });
    </script>
    <?php
    if($ruser[COL_ROLEID] == ROLEADMIN) {
        $this->load->view('layouts/backend-footer');
    } else {
        $this->load->view('layouts/frontend-footer');
    }
    ?>
