<?php
class Schedule extends MY_Controller {
  function __construct() {
      parent::__construct();
  }

  function index() {
    $data['ruser'] = $ruser = GetLoggedUser();
    if(!IsLogin() && $ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('user/login');
    }
    $data['filter'] = $arrfilter = array(
      'DateFrom' => $this->input->get('DateFrom') ? $this->input->get('DateFrom') : date('Y-m-').'01',
      'DateTo' => $this->input->get('DateTo') ? $this->input->get('DateTo') : date('Y-m-t'),
      COL_ID_STATUS => $this->input->get(COL_ID_STATUS) ? $this->input->get(COL_ID_STATUS) : '',
      COL_ID_APPOINTMENT => $this->input->get(COL_ID_APPOINTMENT) ? $this->input->get(COL_ID_APPOINTMENT) : '',
      'Keyword' => $this->input->get('Keyword') ? $this->input->get('Keyword') : ''
    );

    $data['title'] = "Permintaan Jasa";
    $this->db
        ->select('*, um.Uniq as ID_Member, um.NM_FullName as NM_Member, up.Uniq as ID_Provider, up.NM_FullName as NM_Psikolog, t_appointment.CreatedBy as CreatedBy, t_appointment.CreatedOn as CreatedOn')
        ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_ID_TYPE." = ".TBL_T_APPOINTMENT.".".COL_ID_TYPE,"left")
        ->join(TBL_USERINFORMATION." up ",'up.'.COL_USERNAME." = ".TBL_T_APPOINTMENT.".".COL_ID_PSIKOLOG,"left")
        ->join(TBL_USERINFORMATION." um ",'um.'.COL_USERNAME." = ".TBL_T_APPOINTMENT.".".COL_ID_PATIENT,"left")
        ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_T_APPOINTMENT.".".COL_ID_STATUS,"left");

    if($ruser[COL_ROLEID]==ROLEMEMBER) {
      $this->db->where(TBL_T_APPOINTMENT.".".COL_ID_PATIENT, $ruser[COL_USERNAME]);
    } else if($ruser[COL_ROLEID]==ROLEPROVIDER) {
      $this->db->where(TBL_T_APPOINTMENT.".".COL_ID_PSIKOLOG, $ruser[COL_USERNAME]);
    }
    $this->db->where(TBL_T_APPOINTMENT.'.'.COL_DATE_SCHEDULE.' >= ', $arrfilter['DateFrom']);
    $this->db->where(TBL_T_APPOINTMENT.'.'.COL_DATE_SCHEDULE.' <= ', $arrfilter['DateTo']);
    if(!empty($arrfilter[COL_ID_STATUS])) {
      $this->db->where(TBL_T_APPOINTMENT.'.'.COL_ID_STATUS, $arrfilter[COL_ID_STATUS]);
    }
    if(!empty($arrfilter[COL_ID_APPOINTMENT])) {
      $this->db->like(TBL_T_APPOINTMENT.'.'.COL_ID_APPOINTMENT, ltrim($arrfilter[COL_ID_APPOINTMENT],'0'));
    }
    if(!empty($arrfilter['Keyword'])) {
      $this->db->group_start();
      $this->db->like("up.NM_FullName", $arrfilter['Keyword']);
      $this->db->or_like("um.NM_FullName as NM_Member", $arrfilter['Keyword']);
      $this->db->or_like(TBL_MTYPE.'.'.COL_NM_TYPE, $arrfilter['Keyword']);
      $this->db->group_end();
    }
    $data['res'] = $this->db->get(TBL_T_APPOINTMENT)->result_array();
    $this->load->view('schedule/index', $data);
  }

  function add($isajax=0, $idpsikolog=0) {
      $data['ruser'] = $ruser = GetLoggedUser();
      $data['title'] = "Permintaan Jasa";
      $data['edit'] = FALSE;
      $data['forcepsikolog'] = FALSE;
      $data['isajax'] = $isajax;

      if($idpsikolog!=0) {
        $rpsikolog = $this->db->where(COL_UNIQ, $idpsikolog)->get(TBL_USERINFORMATION)->row_array();
        if(!empty($rpsikolog)) {
          $data['forcepsikolog'] = TRUE;
          $data['data']["NM_Psikolog"] = $rpsikolog[COL_NM_FULLNAME];
          $data['data'][COL_ID_PSIKOLOG] = $rpsikolog[COL_USERNAME];
        }
      }

      if(!empty($_POST)) {
          $data['data'] = $_POST;
          $stat_ = $this->input->post(COL_ID_STATUS);
          $member_ = $this->input->post(COL_ID_PATIENT);
          $psikolog_ = $this->input->post(COL_ID_PSIKOLOG);
          if(empty($stat_)) {
              $stat_ = STATUS_MENUNGGU;
          }
          if($ruser[COL_ROLEID]==ROLEMEMBER) {
              $member_ = $ruser[COL_USERNAME];
          }
          if($ruser[COL_ROLEID]==ROLEPROVIDER) {
              $psikolog_ = $ruser[COL_USERNAME];
          }
          $rec = array(
              COL_ID_TYPE => $this->input->post(COL_ID_TYPE),
              COL_ID_PSIKOLOG => $psikolog_,
              COL_ID_PATIENT => $member_,
              COL_ID_STATUS => $stat_,
              COL_DATE_SCHEDULE => $this->input->post(COL_DATE_SCHEDULE),
              COL_NOTE_PATIENT => $this->input->post(COL_NOTE_PATIENT),
              COL_NOTE_PSIKOLOG => $this->input->post(COL_NOTE_PSIKOLOG),

              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
          );
          $this->db->trans_begin();
          try {
              $res = $this->db->insert(TBL_T_APPOINTMENT, $rec);
              if($res) {
                  $this->db->trans_commit();
                  if($isajax) {
                    ShowJsonSuccess('Berhasil menambah jadwal.');
                    return;
                  } else {
                    if($ruser[COL_ROLEID]==ROLEADMIN) redirect('schedule/index');
                    else redirect('schedule/my-schedule');
                  }
              } else {
                if($isajax) {
                  ShowJsonError('Gagal menambah permintaan.');
                  return;
                } else {
                  $this->db->trans_rollback();
                  redirect(current_url()."?error=1");
                }
              }
          } catch(Exception $ex) {
            if($isajax) {
              ShowJsonError($ex->Message);
              return;
            } else {
              $this->db->trans_rollback();
              redirect(current_url()."?error=1");
            }
          }
      }
      else {
        if(!$isajax) $this->load->view('schedule/form', $data);
        else $this->load->view('schedule/form_partial', $data);
      }
  }

  function edit($id) {
      $user = GetLoggedUser();
      if(!IsLogin() && $user[COL_ROLEID] != ROLEADMIN) {
          redirect('user/login');
      }

      $rdata = $data['data'] = $this->db->where(COL_ID_APPOINTMENT, $id)->get(TBL_T_APPOINTMENT)->row_array();
      if(empty($rdata)){
          show_404();
          return;
      }
      $data['ruser'] = $user;
      $data['title'] = "Permintaan Jasa";
      $data['edit'] = TRUE;
      $data['forcepsikolog'] = FALSE;
      $data['isajax'] = FALSE;
      if(!empty($_POST)){
          $data['data'] = $_POST;
          $rec = array(
            COL_ID_TYPE => $this->input->post(COL_ID_TYPE),
            COL_ID_PSIKOLOG => $this->input->post(COL_ID_PSIKOLOG),
            COL_ID_PATIENT => $this->input->post(COL_ID_PATIENT),
            COL_ID_STATUS => $this->input->post(COL_ID_STATUS),
            COL_DATE_SCHEDULE => $this->input->post(COL_DATE_SCHEDULE),
            COL_NOTE_PATIENT => $this->input->post(COL_NOTE_PATIENT),
            COL_NOTE_PSIKOLOG => $this->input->post(COL_NOTE_PSIKOLOG),
            COL_UPDATEDBY => $user[COL_USERNAME],
            COL_UPDATEDON => date('Y-m-d H:i:s')
          );
          $this->db->trans_begin();
          try {
              $res = $this->db->where(COL_ID_ORDER, $id)->update(TBL_TORDER, $rec);
              if($res) {
                  $this->db->trans_commit();
                  redirect('schedule/index');
              } else {
                  $this->db->trans_rollback();
                  redirect(current_url()."?error=1");
              }
          } catch(Exception $ex) {
              $this->db->trans_rollback();
              redirect(current_url()."?error=1");
          }
      }
      else {
          $this->load->view('schedule/form', $data);
      }
  }

  function delete(){
      if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
          $this->db->delete(TBL_T_APPOINTMENT, array(COL_ID_APPOINTMENT => $datum));
          $deleted++;
      }
      if($deleted){
          ShowJsonSuccess($deleted." data dihapus");
      }else{
          ShowJsonError("Tidak ada dihapus");
      }
  }

  function status_edit($id){
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPROVIDER)) {
        ShowJsonError('Akses ditolak.');
        return;
      }

      $ruser = GetLoggedUser();
      $rdata = $this->db->where(COL_ID_APPOINTMENT, $id)->get(TBL_T_APPOINTMENT)->row_array();
      if(empty($rdata)) {
        ShowJsonError('Permintaan tidak valid.');
        return;
      }

      $stat_ = $this->input->post(COL_ID_STATUS);
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_ID_APPOINTMENT, $id)->update(TBL_T_APPOINTMENT, array(
          COL_ID_STATUS => $stat_
        ));

        if($res) {
          $this->db->trans_commit();
          ShowJsonSuccess('Status berhasil diubah.');
          return;
        } else {
          $this->db->trans_rollback();
          $this->db->trans_commit();
          ShowJsonError('Status gagal diubah.');
          return;
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError('Server Problem.');
        return;
      }
  }

  function detail($id) {
    $rdata = $this->db
    ->select("t_appointment.*, mstatus.NM_Status, mstatus.NM_Color, mtype.NM_Type, up.NM_FullName as NM_Provider, up.Uniq as ID_Provider, um.NM_FullName as NM_Member, um.Uniq as ID_Member")
    ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_ID_TYPE." = ".TBL_T_APPOINTMENT.".".COL_ID_TYPE,"left")
    ->join(TBL_USERINFORMATION." up ",'up.'.COL_USERNAME." = ".TBL_T_APPOINTMENT.".".COL_ID_PSIKOLOG,"left")
    ->join(TBL_USERINFORMATION." um ",'um.'.COL_USERNAME." = ".TBL_T_APPOINTMENT.".".COL_ID_PATIENT,"left")
    ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_T_APPOINTMENT.".".COL_ID_STATUS,"left")
    ->where(COL_ID_APPOINTMENT, $id)
    ->get(TBL_T_APPOINTMENT)
    ->row_array();
    if(empty($rdata)){
        show_404();
        return;
    }
    $data['data'] = $rdata;
    $data['title'] = '#'.str_pad($rdata[COL_ID_APPOINTMENT], 5, '0', STR_PAD_LEFT);

    $this->load->view('schedule/detail', $data);
  }

  function comments($id) {
    $user = GetLoggedUser();
    $msgs = $this->db
    ->select('t_appointment_feedback.*, userinformation.NM_FullName')
    ->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_T_APPOINTMENT_FEEDBACK.".".COL_CREATEDBY,"left")
    ->where(COL_ID_APPOINTMENT, $id)
    ->where(COL_TYPE, 'COMMENT')
    ->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_T_APPOINTMENT_FEEDBACK)
    ->result_array();
    $html = '';
    $html .= '<div class="direct-chat-messages" style="height: 50vh !important">';
    if(empty($msgs)) {
      $html .= '<p class="text-muted">Belum ada catatan / komentar</p>';
    } else {
      $last_user = '';
      $last_time = '';
      foreach($msgs as $m) {
        if($m[COL_CREATEDBY]==$user[COL_USERNAME]) {
          $html .= '<div class="direct-chat-msg right">';
          if($m[COL_NM_FULLNAME] != $last_user || date('d M Y  H:i', strtotime($m[COL_CREATEDON])) != $last_time) {
            $html .= '<div class="direct-chat-infos clearfix">';
            $html .= '<span class="direct-chat-name float-right">Anda</span>';
            $html .= '<span class="direct-chat-timestamp float-left">'.date('d M Y  H:i', strtotime($m[COL_CREATEDON])).'</span>';
            $html .= '</div>';
          }
          //$html .= '<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">';
          $html .= '<div class="direct-chat-text mr-0">';
          $html .= $m[COL_COMMENT];
          $html .= '</div>';
          $html .= '</div>';
        } else {
          $html .= '<div class="direct-chat-msg left">';
          if($m[COL_NM_FULLNAME] != $last_user || date('d M Y  H:i', strtotime($m[COL_CREATEDON])) != $last_time) {
            $html .= '<div class="direct-chat-infos clearfix">';
            $html .= '<span class="direct-chat-name float-left">'.$m[COL_NM_FULLNAME].'</span>';
            $html .= '<span class="direct-chat-timestamp float-right">'.date('d M Y  H:i', strtotime($m[COL_CREATEDON])).'</span>';
            $html .= '</div>';
          }
          //$html .= '<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">';
          $html .= '<div class="direct-chat-text ml-0">';
          $html .= $m[COL_COMMENT];
          $html .= '</div>';
          $html .= '</div>';
        }
        $last_user = $m[COL_NM_FULLNAME];
        $last_time = date('d M Y  H:i', strtotime($m[COL_CREATEDON]));
      }
    }
    $html .= '</div>';
    $html .= '<script>$(document).ready(function() { scrollMessages(); });</script>';
    echo $html;
  }

  function comment_add($id, $rate=0) {
    $user = GetLoggedUser();
    $msg = $this->input->post(COL_COMMENT);
    $dat = array(
      COL_ID_APPOINTMENT=>$id,
      COL_TYPE=>'COMMENT',
      COL_COMMENT=>$msg,
      COL_CREATEDBY=>$user[COL_USERNAME],
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );
    if($rate != 0) {
      $dat[COL_RATE] = $this->input->post(COL_RATE);
      $dat[COL_TYPE] = 'RATE';
    }
    $res = $this->db->insert(TBL_T_APPOINTMENT_FEEDBACK, $dat);
    if($res) {
      ShowJsonSuccess('OK');
    } else {
      ShowJsonError('Server error.');
    }
  }
}
 ?>
