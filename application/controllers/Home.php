<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('mpost');
    }

    public function index()
    {
        /*if(!IsLogin()) {
            redirect('user/login');
        }
        redirect('user/dashboard');*/

        $keyword = $this->input->get(COL_NM_FULLNAME);
        $prov = $this->input->get(COL_NM_PROVINSI);
        $city = $this->input->get(COL_NM_KOTA);
        $service = $this->input->get(COL_ID_TYPE);

        $this->db->select(TBL_USERINFORMATION.".*,".TBL_USERS.".*,userinformation.Uniq as ID_Provider");
        $this->db->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_USERINFORMATION.".".COL_USERNAME,"left");
        $this->db->join(TBL_SERVICE,TBL_SERVICE.'.'.COL_USERNAME." = ".TBL_USERINFORMATION.".".COL_USERNAME,"left");
        $this->db->join(TBL_SERVICE_AREA,TBL_SERVICE_AREA.'.'.COL_USERNAME." = ".TBL_USERINFORMATION.".".COL_USERNAME,"left");
        $this->db->where(TBL_USERS.".".COL_ROLEID, ROLEPROVIDER);
        if(!empty($keyword)) {
          $this->db->like(COL_NM_FULLNAME, $keyword);
        }
        if(!empty($prov)) {
          $this->db->where(TBL_SERVICE_AREA.".".COL_NM_PROVINSI, $prov);
        }
        if(!empty($city)) {
          $this->db->where(TBL_SERVICE_AREA.".".COL_NM_KOTA, $city);
        }
        if(!empty($service)) {
          $this->db->where(TBL_SERVICE.".".COL_ID_TYPE, $service);
        }
        $this->db->order_by(TBL_USERINFORMATION.".".COL_DATE_REGISTERED, 'desc');
        $this->db->group_by(TBL_USERINFORMATION.".".COL_USERNAME);
        $data['rpsikolog'] = $this->db->get(TBL_USERINFORMATION)->result_array();
        $data['title'] = 'Selamat Datang';
        $data['filter'] = array(
          COL_NM_FULLNAME=>$keyword,
          COL_NM_PROVINSI=>$prov,
          COL_NM_KOTA=>$city,
          COL_ID_TYPE=>$service
        );
        $this->load->view('home/index', $data);
    }

    public function index_2() {
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(4,"",1);
        $data['gallery'] = $this->mpost->search(4,"",4);
        $this->load->view('home/index_2', $data);
    }

    function _404() {
        $this->load->view('home/error');
    }
}
