<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 12:50
 */
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function status_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Status";
        $data['res'] = $this->db->get(TBL_MSTATUS)->result_array();
        $this->load->view('master/index_status', $data);
    }

    function status_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_STATUS => $this->input->post(COL_NM_STATUS),
                COL_NM_COLOR => $this->input->post(COL_NM_COLOR)
            );

            $res = $this->db->insert(TBL_MSTATUS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function status_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_STATUS => $this->input->post(COL_NM_STATUS),
                COL_NM_COLOR => $this->input->post(COL_NM_COLOR)
            );

            $res = $this->db->where(COL_ID_STATUS, $id)->update(TBL_MSTATUS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function status_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MSTATUS, array(COL_ID_STATUS => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function type_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Tipe Kunjungan";
        $data['res'] = $this->db->get(TBL_MTYPE)->result_array();
        $this->load->view('master/index_type', $data);
    }

    function type_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_TYPE => $this->input->post(COL_NM_TYPE)
            );

            $res = $this->db->insert(TBL_MTYPE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function type_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_TYPE => $this->input->post(COL_NM_TYPE)
            );

            $res = $this->db->where(COL_ID_TYPE, $id)->update(TBL_MTYPE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function type_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MTYPE, array(COL_ID_TYPE => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function service_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Tipe Service";
        $data['res'] = $this->db->get(TBL_MSERVICE)->result_array();
        $this->load->view('master/index_service', $data);
    }

    function service_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_SERVICE => $this->input->post(COL_NM_SERVICE),
                COL_DURATION => $this->input->post(COL_DURATION),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MSERVICE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function service_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_SERVICE => $this->input->post(COL_NM_SERVICE),
                COL_DURATION => $this->input->post(COL_DURATION),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_ID_SERVICE, $id)->update(TBL_MSERVICE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function service_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MSERVICE, array(COL_ID_SERVICE => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function customer_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Konsumen & Kendaraan";
        $data['res'] = $this->db->get(TBL_MCUSTOMER)->result_array();
        $this->load->view('master/index_customer', $data);
    }

    function customer_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Konsumen & Kendaraan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NM_CUSTOMER => $this->input->post(COL_NM_CUSTOMER),
                COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
                COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
                COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_MCUSTOMER, $rec);

                $id_customer = $this->db->insert_id();
                $vehicle = array(
                    COL_ID_CUSTOMER => $id_customer,
                    COL_ID_TYPE => $this->input->post(COL_ID_TYPE),
                    COL_NM_PEMILIK => $this->input->post(COL_NM_PEMILIK),
                    COL_NO_PLAT => $this->input->post(COL_NO_PLAT),
                    COL_TH_PEMBUATAN => $this->input->post(COL_TH_PEMBUATAN),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );
                $res2 = $this->db->insert(TBL_MVEHICLE, $vehicle);

                if($res && $res2) {
                    $this->db->trans_commit();
                    redirect('master/customer-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/form_customer', $data);
        }
    }

    function customer_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_ID_CUSTOMER, $id)->get(TBL_MCUSTOMER)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
            $data['title'] = "Konsumen & Kendaraan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NM_CUSTOMER => $this->input->post(COL_NM_CUSTOMER),
                COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
                COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
                COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->where(COL_ID_CUSTOMER, $id)->update(TBL_MCUSTOMER, $rec);
                if($res) {
                    $this->db->trans_commit();
                    redirect('master/customer-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/form_customer', $data);
        }
    }

    function customer_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MCUSTOMER, array(COL_ID_CUSTOMER => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function customer_vehicles($id) {
        $data['data'] = array(COL_ID_CUSTOMER=>$id);
        if($this->input->is_ajax_request()) {
            $this->load->view('master/index_customer_vehicles', $data);
        } else {
            echo "Under development.";
        }
    }

    function customer_vehicle_add($id=null,$ajax=0) {
        $user = GetLoggedUser();
        $data['title'] = "Task";
        $data['edit'] = FALSE;
        $data['data'] = array(
            COL_ID_CUSTOMER => !empty($id)?$id:null,
            "IsAjax" => $ajax
        );

        if(!empty($_POST)){
            $ajax = $this->input->post("IsAjax");
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_CUSTOMER => $this->input->post(COL_ID_CUSTOMER),
                COL_ID_TYPE => $this->input->post(COL_ID_TYPE),
                COL_NM_PEMILIK => $this->input->post(COL_NM_PEMILIK),
                COL_NO_PLAT => $this->input->post(COL_NO_PLAT),
                COL_TH_PEMBUATAN => $this->input->post(COL_TH_PEMBUATAN),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_MVEHICLE, $rec);

                if($res) {
                    $this->db->trans_commit();
                    if($ajax) {
                        echo json_encode(array("error"=>0));
                        return;
                    } else {
                        echo "Bad request.";
                        return;
                    }
                } else {
                    $this->db->trans_rollback();
                    if($ajax) {
                        echo json_encode(array("error"=>"Server Error."));
                        return;
                    } else {
                        redirect(current_url()."?error=1");
                    }
                }
            } catch(Exception $e) {
                $this->db->trans_rollback();
                if($ajax) {
                    echo json_encode(array("error"=>"Server Error."));
                    return;
                } else {
                    redirect(current_url()."?error=1");
                }
            }
        }
        else {
            if($this->input->is_ajax_request()) {
                $this->load->view('master/form_customer_vehicles', $data);
            }
        }
    }

    function customer_vehicle_edit($id=null,$ajax=0) {
        $user = GetLoggedUser();
        $data['title'] = "Kendaraan";
        $data['edit'] = TRUE;

        $rdata = $data['data'] = $this->db
            ->select("mvehicle.*, mvehicletype.NM_Type")
            ->join(TBL_MVEHICLETYPE,TBL_MVEHICLETYPE.'.'.COL_ID_TYPE." = ".TBL_MVEHICLE.".".COL_ID_TYPE,"left")
            ->where(COL_ID_VEHICLE, $id)
            ->get(TBL_MVEHICLE)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['data']['IsAjax'] = $ajax;
        if(!empty($_POST)){
            $ajax = $this->input->post("IsAjax");
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_CUSTOMER => $this->input->post(COL_ID_CUSTOMER),
                COL_ID_TYPE => $this->input->post(COL_ID_TYPE),
                COL_NM_PEMILIK => $this->input->post(COL_NM_PEMILIK),
                COL_NO_PLAT => $this->input->post(COL_NO_PLAT),
                COL_TH_PEMBUATAN => $this->input->post(COL_TH_PEMBUATAN),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $this->db->trans_begin();
            try {
                $res = $this->db->where(COL_ID_VEHICLE, $id)->update(TBL_MVEHICLE, $rec);

                if($res) {
                    $this->db->trans_commit();
                    if($ajax) {
                        echo json_encode(array("error"=>0));
                        return;
                    }
                } else {
                    $this->db->trans_rollback();
                    if($ajax) {
                        echo json_encode(array("error"=>"Server Error."));
                        return;
                    } else {
                        redirect(current_url()."?error=1");
                    }
                }
            } catch(Exception $e) {
                $this->db->trans_rollback();
                if($ajax) {
                    echo json_encode(array("error"=>"Server Error."));
                    return;
                } else {
                    redirect(current_url()."?error=1");
                }
            }
        } else {
            if($this->input->is_ajax_request()) {
                $this->load->view('master/form_customer_vehicles', $data);
            }
        }
    }

    function customer_vehicle_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MVEHICLE, array(COL_ID_VEHICLE => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function vehicletype_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Tipe Kendaraan";
        $data['res'] = $this->db->get(TBL_MVEHICLETYPE)->result_array();
        $this->load->view('master/index_vehicletype', $data);
    }

    function vehicletype_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_TYPE => $this->input->post(COL_NM_TYPE),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MVEHICLETYPE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function vehicletype_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){

            $data['data'] = $_POST;
            $data = array(
                COL_NM_TYPE => $this->input->post(COL_NM_TYPE),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_ID_TYPE, $id)->update(TBL_MVEHICLETYPE, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function vehicletype_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MVEHICLETYPE, array(COL_ID_TYPE => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function mechanic_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Mekanik";
        $data['res'] = $this->db->get(TBL_MMECHANIC)->result_array();
        $this->load->view('master/index_mechanic', $data);
    }

    function mechanic_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Mekanik";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NM_MECHANIC => $this->input->post(COL_NM_MECHANIC),
                COL_TH_BERGABUNG => $this->input->post(COL_TH_BERGABUNG),
                COL_NO_TELP => $this->input->post(COL_NO_TELP),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_MMECHANIC, $rec);

            if($res) {
                redirect('master/mechanic-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/form_mechanic', $data);
        }
    }

    function mechanic_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_ID_MECHANIC, $id)->get(TBL_MMECHANIC)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Mechanic";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NM_MECHANIC => $this->input->post(COL_NM_MECHANIC),
                COL_TH_BERGABUNG => $this->input->post(COL_TH_BERGABUNG),
                COL_NO_TELP => $this->input->post(COL_NO_TELP),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->where(COL_ID_MECHANIC, $id)->update(TBL_MMECHANIC, $rec);
            if($res) {
                redirect('master/mechanic-index');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/form_mechanic', $data);
        }
    }

    function mechanic_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MMECHANIC, array(COL_ID_MECHANIC => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function get_opt_vehicle() {
        $idcustomer = $this->input->post(COL_ID_CUSTOMER);

        $res = GetCombobox("SELECT * FROM mvehicle left join mvehicletype t on t.ID_Type = mvehicle.ID_Type where ID_Customer = ".$idcustomer." ORDER BY t.NM_Type", COL_ID_VEHICLE, array(COL_NO_PLAT, COL_NM_TYPE), null, false, true);
        echo $res;
    }
}
