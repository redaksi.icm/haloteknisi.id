<?php
$dbpconnect = TRUE;
ini_set('upload_max_filesize', '50M');
ini_set('post_max_size', '50M');


//MY_CONSTANT
define('MY_BASEURL', 'http://localhost/web-haloteknisi/');
define('MY_DBHOST', 'localhost');
define('MY_DBUSER', 'root');
define('MY_DBPASS', '');
define('MY_DBNAME', 'web_haloteknisi_20200531');
define('MY_DBPCONNECT', $dbpconnect);
define('MY_ASSETPATH', './assets');
define('MY_ASSETURL', MY_BASEURL.'/assets');
define('MY_IMAGEPATH', './assets/media/image/');
define('MY_IMAGEURL', MY_ASSETURL.'/media/image/');
define('MY_UPLOADPATH', './assets/media/upload/');
define('MY_UPLOADURL', MY_ASSETURL.'/media/upload/');
define('MY_NOIMAGEURL', MY_ASSETURL.'/media/image/no-image.png');
define('MY_NODATAURL', MY_ASSETURL.'/media/image/no-data.png');
define('SYSTEMUSERNAME', 'admin');

define('FRONTENDURL', MY_ASSETURL.'/frontend');
define('FRONTENDPATH', './assets/frontend');
define('FRONTENDVIEWPATH', FRONTENDPATH.'/view');

define("URL_SUFFIX", ".jsp");

define("UPLOAD_ALLOWEDTYPES", "gif|jpg|jpeg|png|doc|docx|txt|pdf");

define('CONS_PTKP_PLUS', 63000000);
define('CONS_PTKP_MUL', 4000000);
define('PPH_5', 50000000);

define('STATUS_MENUNGGU', 1);
define('STATUS_DITERIMA', 2);
define('STATUS_SELESAI', 3);
define('STATUS_BATAL', 3);
