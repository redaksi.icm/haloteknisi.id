# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: web_haloteknisi_20200531
# Generation Time: 2020-06-13 16:43:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `ID_Status` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Status` varchar(50) NOT NULL DEFAULT '',
  `NM_Color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mstatus` WRITE;
/*!40000 ALTER TABLE `mstatus` DISABLE KEYS */;

INSERT INTO `mstatus` (`ID_Status`, `NM_Status`, `NM_Color`)
VALUES
	(1,'MENUNGGU','#CCCCCC'),
	(2,'DITERIMA','#FFFF99'),
	(3,'SELESAI','#99FFCC'),
	(4,'DIBATALKAN','#FFCCCC');

/*!40000 ALTER TABLE `mstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtype`;

CREATE TABLE `mtype` (
  `ID_Type` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Type` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID_Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mtype` WRITE;
/*!40000 ALTER TABLE `mtype` DISABLE KEYS */;

INSERT INTO `mtype` (`ID_Type`, `NM_Type`)
VALUES
	(1,'Ahli Kunci'),
	(2,'Alarm & CCTV'),
	(3,'Arsitek'),
	(4,'Bengkel Las'),
	(5,'Pemasangan / Perbaikan AC'),
	(6,'Pemasangan / Perbaikan Lampu'),
	(8,'Service AC'),
	(9,'Service Kulkas'),
	(10,'Service Mesin Cuci'),
	(11,'Service Pompa Air'),
	(12,'Service TV');

/*!40000 ALTER TABLE `mtype` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `postcategories` WRITE;
/*!40000 ALTER TABLE `postcategories` DISABLE KEYS */;

INSERT INTO `postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Berita','#f56954'),
	(5,'Others','#3c8dbc');

/*!40000 ALTER TABLE `postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Penyedia Jasa'),
	(3,'Member');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `service`;

CREATE TABLE `service` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `ID_Type` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;

INSERT INTO `service` (`Uniq`, `UserName`, `ID_Type`)
VALUES
	(26,'cvindoteknisi@haloteknisi.id',5),
	(27,'cvindoteknisi@haloteknisi.id',6),
	(28,'cvindoteknisi@haloteknisi.id',8),
	(29,'cvindoteknisi@haloteknisi.id',9),
	(30,'cvindoteknisi@haloteknisi.id',10),
	(31,'cvindoteknisi@haloteknisi.id',11),
	(213,'medanteknik@gmail.com',5),
	(214,'medanteknik@gmail.com',8),
	(215,'medanteknik@gmail.com',9),
	(218,'gabemateknik@gmail.com',5),
	(219,'gabemateknik@gmail.com',8),
	(220,'sumutteknisi@gmail.com',5),
	(221,'sumutteknisi@gmail.com',8),
	(222,'sumutteknisi@gmail.com',9),
	(223,'sumutteknisi@gmail.com',10);

/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table service_area
# ------------------------------------------------------------

DROP TABLE IF EXISTS `service_area`;

CREATE TABLE `service_area` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `NM_Provinsi` varchar(50) NOT NULL DEFAULT '',
  `NM_Kota` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `service_area` WRITE;
/*!40000 ALTER TABLE `service_area` DISABLE KEYS */;

INSERT INTO `service_area` (`Uniq`, `UserName`, `NM_Provinsi`, `NM_Kota`)
VALUES
	(8,'cvindoteknisi@haloteknisi.id','SUMATERA UTARA','KOTA BINJAI'),
	(9,'cvindoteknisi@haloteknisi.id','SUMATERA UTARA','KOTA MEDAN'),
	(10,'cvindoteknisi@haloteknisi.id','SUMATERA BARAT','KOTA PADANG'),
	(11,'medanteknik@gmail.com','SUMATERA UTARA','KOTA MEDAN'),
	(13,'gabemateknik@gmail.com','SUMATERA UTARA','KOTA MEDAN'),
	(14,'sumutteknisi@gmail.com','SUMATERA UTARA','KOTA MEDAN'),
	(15,'sumutteknisi@gmail.com','SUMATERA UTARA','KOTA BINJAI');

/*!40000 ALTER TABLE `service_area` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','HALOTEKNISI.ID'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Platform Teknisi Indonesia'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_appointment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_appointment`;

CREATE TABLE `t_appointment` (
  `ID_Appointment` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Type` bigint(10) unsigned NOT NULL,
  `ID_Psikolog` varchar(50) NOT NULL DEFAULT '',
  `ID_Patient` varchar(50) NOT NULL DEFAULT '',
  `ID_Status` bigint(10) NOT NULL,
  `DATE_Schedule` datetime NOT NULL,
  `DATE_Reschedule` int(11) DEFAULT NULL,
  `Note_Patient` varchar(50) DEFAULT NULL,
  `Note_Psikolog` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Appointment`),
  KEY `FK_Type` (`ID_Type`),
  CONSTRAINT `FK_Type` FOREIGN KEY (`ID_Type`) REFERENCES `mtype` (`ID_Type`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `t_appointment` WRITE;
/*!40000 ALTER TABLE `t_appointment` DISABLE KEYS */;

INSERT INTO `t_appointment` (`ID_Appointment`, `ID_Type`, `ID_Psikolog`, `ID_Patient`, `ID_Status`, `DATE_Schedule`, `DATE_Reschedule`, `Note_Patient`, `Note_Psikolog`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,9,'cvindoteknisi@haloteknisi.id','rolassimanjuntak@gmail.com',3,'2020-04-12 00:00:00',NULL,'KULKAS.',NULL,'rolassimanjuntak@gmail.com','2020-04-11 13:30:42',NULL,NULL),
	(10,10,'cvindoteknisi@haloteknisi.id','rolassimanjuntak@gmail.com',3,'2020-04-13 00:00:00',NULL,'',NULL,'rolassimanjuntak@gmail.com','2020-04-12 11:03:51',NULL,NULL),
	(14,11,'cvindoteknisi@haloteknisi.id','rolassimanjuntak@gmail.com',3,'2020-04-13 00:00:00',NULL,'DEV',NULL,'rolassimanjuntak@gmail.com','2020-04-12 11:06:34',NULL,NULL),
	(15,5,'cvindoteknisi@haloteknisi.id','parlin@gmail.com',1,'2020-04-15 00:00:00',NULL,'Mau cuci ac dan ganti Obat saja',NULL,'parlin@gmail.com','2020-04-15 13:24:25',NULL,NULL),
	(16,5,'cvindoteknisi@haloteknisi.id','parlin@gmail.com',1,'2020-04-15 00:00:00',NULL,'ganti obat dan sparepart ',NULL,'parlin@gmail.com','2020-04-15 13:41:57',NULL,NULL),
	(17,5,'cvindoteknisi@haloteknisi.id','bertosinaga@gmail.com',2,'2020-04-21 00:00:00',NULL,'perbaikan ac dan tambah obat r30',NULL,'bertosinaga@gmail.com','2020-04-21 05:57:42',NULL,NULL),
	(18,5,'cvindoteknisi@haloteknisi.id','parlin@gmail.com',3,'2020-04-21 00:00:00',NULL,'pemasangan ac dan pengisian obat merek r30',NULL,'parlin@gmail.com','2020-04-21 12:59:45',NULL,NULL),
	(19,8,'cvindoteknisi@haloteknisi.id','putridelima@gmail.com',2,'2020-04-21 00:00:00',NULL,'tidak dingin',NULL,'putridelima@gmail.com','2020-04-21 13:35:06',NULL,NULL),
	(20,5,'cvindoteknisi@haloteknisi.id','putri@gmail.com',1,'2020-04-21 00:00:00',NULL,'pemasangan ac dan isi obat',NULL,'putri@gmail.com','2020-04-21 14:21:05',NULL,NULL),
	(21,5,'sumutteknisi@gmail.com','putri12@gmail.com',2,'2020-04-22 00:00:00',NULL,'pasang ac dan isi obat r30',NULL,'putri12@gmail.com','2020-04-21 22:38:20',NULL,NULL),
	(22,5,'sumutteknisi@gmail.com','edisuyoto@gmail.com',1,'2020-05-15 00:00:00',NULL,'Ac saya kurang dingin dan bising suaranya ,,,,than',NULL,'edisuyoto@gmail.com','2020-05-15 14:02:50',NULL,NULL);

/*!40000 ALTER TABLE `t_appointment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table t_appointment_feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_appointment_feedback`;

CREATE TABLE `t_appointment_feedback` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_Appointment` bigint(10) NOT NULL,
  `Type` varchar(10) NOT NULL DEFAULT '',
  `Rate` int(11) DEFAULT NULL,
  `Comment` text,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Appointment` (`ID_Appointment`),
  CONSTRAINT `FK_Appointment` FOREIGN KEY (`ID_Appointment`) REFERENCES `t_appointment` (`ID_Appointment`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `NM_Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_IdentityNo` varchar(50) DEFAULT NULL,
  `DATE_Birth` date DEFAULT NULL,
  `NM_Gender` varchar(50) DEFAULT NULL,
  `NM_Address` text,
  `NM_Provinsi` varchar(200) DEFAULT NULL,
  `NM_Kota` varchar(200) DEFAULT NULL,
  `NM_PhoneNo` varchar(50) DEFAULT NULL,
  `NM_ImageLocation` varchar(250) DEFAULT NULL,
  `NM_About` text,
  `DATE_Registered` date DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userinformation` WRITE;
/*!40000 ALTER TABLE `userinformation` DISABLE KEYS */;

INSERT INTO `userinformation` (`Uniq`, `UserName`, `NM_Email`, `NM_FullName`, `NM_IdentityNo`, `DATE_Birth`, `NM_Gender`, `NM_Address`, `NM_Provinsi`, `NM_Kota`, `NM_PhoneNo`, `NM_ImageLocation`, `NM_About`, `DATE_Registered`)
VALUES
	(1,'admin','yoelrolas@gmail.com','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'085359867032',NULL,NULL,'2018-08-17'),
	(13,'cvindoteknisi@haloteknisi.id','cvindoteknisi@haloteknisi.id','Halo Teknisi',NULL,NULL,NULL,'Jl. Karya No. 50, Kel. Karang Berombak','SUMATERA UTARA','KOTA MEDAN','085359867032',NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2020-04-10'),
	(14,'cvteknisikita@haloteknisi.id','cvteknisikita@haloteknisi.com','Teknisi Kita',NULL,'1970-01-01',NULL,'Jl. Cemara No. 212','SUMATERA UTARA','KOTA MEDAN','085359867032','cvteknisikita.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2020-04-10'),
	(27,'edisuyoto@gmail.com','edisuyoto@gmail.com','edi suyoto',NULL,'1970-01-01',NULL,'jl.Moh Idris gg.Madrasah No.15a Medan Petisah','SUMATERA UTARA','KOTA MEDAN','085359600960','edisuyoto.jpg','Semangat Anak Rantau Pemuda','2020-05-15'),
	(24,'gabemateknik@gmail.com','gabemateknik@gmail.com','GABEMA TEKNIK',NULL,NULL,NULL,'MEDAN','SUMATERA UTARA','KOTA MEDAN','081211221212','gabemateknik.png','GABE MA','2020-04-21'),
	(20,'indoteknik@gmail.com','indoteknik@gmail.com','indo teknik',NULL,'1970-01-01',NULL,'jl.panjajaran no.201A','SUMATERA UTARA','KABUPATEN DELI SERDANG','08116237654',NULL,'kepuasan pelanggan adalah kebahagiaan kita bersama','2020-04-21'),
	(17,'mayasilaen@gmail.com','mayasilaen@gmail.com','maya',NULL,'1970-01-01',NULL,'jl.seirotan no.17a medan','SUMATERA UTARA','KOTA MEDAN','085359600698',NULL,'dekat dengan ramayana sebelah kanan','2020-04-15'),
	(22,'medanteknik@gmail.com','medanteknik@gmail.com','medan teknik',NULL,'1970-01-01',NULL,'jl.gatot subroto no.213 a medan','SUMATERA UTARA','KOTA MEDAN','081234567321','medanteknik.jpg','kepuasaan pelannggan adalah kebahagiaan kami ','2020-04-21'),
	(16,'parlin@gmail.com','parlin@gmail.com','parlindungan ',NULL,NULL,NULL,'jl.tuntungan medan no.16a','SUMATERA UTARA','KOTA MEDAN','081234569870','parlin.jpeg','bangga jadi anak indonesia','2020-04-15'),
	(26,'putri12@gmail.com','putri12@gmail.com','putri ',NULL,'1970-01-01',NULL,'jl.seikambing no.15a','SUMATERA UTARA','KOTA MEDAN','087914234521','putri12.png','jasa service','2020-04-21'),
	(23,'putri@gmail.com','putri@gmail.com','putri delima',NULL,'1970-01-01',NULL,'jl.seikambing no 12a medan kota','SUMATERA UTARA','KOTA MEDAN','081234521233','putri.jpg','penyedia jasa','2020-04-21'),
	(21,'putridelima@gmail.com','putridelima@gmail.com','putri',NULL,'1970-01-01',NULL,'jl.bojonegoro n0.12 A','SUMATERA UTARA','KABUPATEN DELI SERDANG','081234567987',NULL,'Jasa Ac','2020-04-21'),
	(11,'rolassimanjuntak@gmail.com','rolassimanjuntak@gmail.com','Rolas Simanjuntak',NULL,'1970-01-01',NULL,'MEDAN','SUMATERA UTARA','KOTA MEDAN','085359867032',NULL,NULL,'2020-04-10'),
	(25,'sumutteknisi@gmail.com','sumutteknisi@gmail.com','sumut teknisi',NULL,NULL,NULL,'jl.sekip no 28a medan kota','SUMATERA UTARA','KOTA MEDAN','081234567980','sumutteknisi.png','kepuasaaan pelanggan adalah kebahagiaan kami','2020-04-21');

/*!40000 ALTER TABLE `userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2020-06-01 12:51:24','::1'),
	('cvindoteknisi@haloteknisi.id','e10adc3949ba59abbe56e057f20f883e',2,0,'2020-04-13 05:37:25','::1'),
	('cvteknisikita@haloteknisi.id','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	('edisuyoto@gmail.com','983102c4fd83f50e51fa976b75304038',3,0,'2020-05-15 13:53:32','192.168.28.62'),
	('gabemateknik@gmail.com','e10adc3949ba59abbe56e057f20f883e',2,0,'2020-04-21 16:56:38','114.122.7.141'),
	('indoteknik@gmail.com','d54d1702ad0f8326224b817c796763c9',2,0,'2020-04-21 14:32:48','115.178.219.201'),
	('mayasilaen@gmail.com','25f9e794323b453885f5181f1b624d0b',3,0,NULL,NULL),
	('medanteknik@gmail.com','e10adc3949ba59abbe56e057f20f883e',2,0,'2020-04-21 14:58:19','115.178.219.201'),
	('parlin@gmail.com','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-05-08 14:01:18','115.178.202.230'),
	('putri12@gmail.com','d54d1702ad0f8326224b817c796763c9',3,0,'2020-04-21 22:36:35','115.178.222.3'),
	('putri@gmail.com','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-04-21 15:02:02','115.178.219.201'),
	('putridelima@gmail.com','d54d1702ad0f8326224b817c796763c9',3,0,'2020-04-21 14:34:42','115.178.219.201'),
	('rolassimanjuntak@gmail.com','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-06-01 09:50:22','::1'),
	('sumutteknisi@gmail.com','fcea920f7412b5da7be0cf42b8c93759',2,0,'2020-05-15 14:03:27','192.168.28.62');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
